##############################################################
# File: datastruct.tcl
# Version: $_version
# Purpose: Hold various datastructures tcl implementation
# Author: Cosmin IOIART <cosmin.ioiart@bnpparibas.com>
# Copyright: BNP Paribas Arbitrage 
##############################################################



# Simple implementation of tree structure in tcl 
# used for showing the Zones hierarchy
################################################
proc traverse {tree {prefix ""}} {
	set res {}
	if {[llength $tree]>1} {
		lappend res [concat $prefix 0] ;# content
		set i 0
		foreach child [lrange $tree 1 end] {
			eval lappend res [traverse $child [concat $prefix [incr i]]]
		}
	} else {set res [list $prefix]} ;# leaf
	set res
}

proc addSubtree {tree index subtree} {
	if {[lindex $index end]==0} {set index [lrange $index 0 end-1]}
	set node [lindex $tree $index]
	lappend node $subtree
	lset tree $index $node
	set tree
}

proc absolutePath {tree index} {
	set res {}
	foreach i [fromRoot $index] {
		lappend res [lindex $tree $i]
	}
	set res
}

proc fromRoot index {
	set res {}
	set path {}
	foreach i $index {
		if $i {lappend res [concat $path 0]}
		lappend path $i
	}
	lappend res $index
}

proc search { mytree {srcterm ""}} {	
	if { [string length $srcterm] == "0" } {		
		return -1
	}
	
	foreach i [traverse $mytree] {		
		if {[lindex $mytree $i] == $srcterm} {			
			return $i
		}
	}
	return -1
}
################################################
# end of tree structure implementation