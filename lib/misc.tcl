##############################################################
# File: misc.tcl 
# Purpose: Contains worker dispatch procedures and auxiliary
#				procedures not directly linked to Centrify's adedit
# Author: Cosmin IOIART <cosmin.ioiart@bnpparibas.com>
# Copyright: BNP Paribas Arbitrage 
##############################################################

set domaindn ""
set zonelist  ""

proc printRed {{msg ""}} {
	puts -nonewline "\033\[31m$msg\033\[0m"
}

proc printGreen {{msg ""}} {
	puts -nonewline "\033\[32m$msg\033\[0m"
}

#----------------------------------------------------------------------------------
# Extracts the zone name from a zone DN
proc extractZoneFromDN {zone} {
	set zone [split $zone ","]
	set zone [lindex $zone 0]
	set zone [split $zone "="]
	set zone [lindex $zone 1]
	return $zone
}

#----------------------------------------------------------------------------------

proc getDomDN { adobject } {
	global domain

	if {[string first "@" $adobject] == -1 } {
		set adobject "$adobject@$domain"
	}
	set domName [lindex [split $adobject "@" ] 1]
	return [dn_from_domain $domName]
}

proc domainFix { aduser } {
	global domain

	if {[string first "@" $aduser] != -1 } {
		return $aduser
	} else {
		return "$aduser@$domain"
	}
}


proc getSAMAcctName { adobject } {
	
	if { [string first "@" $adobject] != -1} {
		set adobject [lindex [split $adobject "@" ] 0]
	}
	
	# Removing first { and last } from object name
	
	set ndx [string first "\{" $adobject]	
	set adobject [string replace $adobject $ndx $ndx ]	
	set ndx [string last "\}" $adobject]	
	set adobject [string replace $adobject $ndx $ndx ]
	
	return $adobject
}


proc checkParams {_argv} {
	# ignoring all the -<whatever> parameters
	set _paramslst [lsearch -all -regexp $_argv "\-\[A-Z\]|\-\[a-z\]"]
	if { [llength $_paramslst] > 0 } {
		puts stderr "Too many command line options specified"
		exit 1
	}
}

proc printTitle { str } {
	set len [string length $str ]
	puts "$str"
	for { set i 0 } {$i < $len} {incr i} {
		puts -nonewline "="
	}
	puts ""
}

proc usage { {msg ""} } {
	global _EXENAME
	global _copyright
	puts "\n$_copyright"
	for {set i 0} {$i < [string length $_copyright]} {incr i} {
		puts -nonewline "-"
	}
	puts ""
	if { [string length $msg] > 0 } {
		puts stderr "$msg"
	}
	puts ""

	puts "Usage: $_EXENAME -d <domain> -l <aduser> \[-p password file\] -c <command> <cmd args>"
	puts "\t <domain> - Active Directory domain name (e.g. gaia.net.intra)"
	puts "\t <aduser> - AD account to perform <command> as"
	puts "\t -p       - optional file containing password for <aduser>"
	puts "\t <command>   - the command (operation) to execute"
	puts "\t <cmd agrs>  - arguments required for the command"
	puts "\nAvailable commands:"
	puts "\t help"
	puts "\t adprepare <srv list> - creates a zone, user provisioning group, computer account etc"
	puts "\t precreate \-z <zone> \-s <srv_name>"
	puts "\t decomzones <srv list> - destroys a zone including computer accounts and users"
	puts "\t activateZPA - Configure/Activate ZPA for GlobalZone"
	puts "\t deactivateZPA - Deactivate ZPA for GlobalZone"
	puts "\t listzones \[<src string>\] - Displays a list of zones optionally filtered by <src string>"
	puts "\t listauth \[assignment|roles|commands\] <srv name> - List authorizations for a zone"
	puts "\t createcmd \-C <command> \[\-P <PATH>\] \[\-D description\] - Creates new command for authorization"
	puts "\t deletecmd \-C <command> <srv list> - Removes an existing authorization command"
	puts "\t modifycmd \-A <cmd alias> \[\-C <new command>\] \[\-P <new PATH>\] \[\-D <new description>\] <srv list> - Modifies an authorization command"
	puts "\t searchcmd \-C <command> <srv name"
	puts "\t createrole \-R <role_name> \[\-D <description> \]<srv list> - Creates a new role"
	puts "\t deleterole \-R <role_name> <srv list> - Deletes an existing role"
	puts "\t assigncmd \-R <role_name> \-C <command alias> \[\-C <command alias> ...\] <srv list> - Assigns one or more commands to a role"
	puts "\t assignrole \-R <role name> \-T <AD target> \[-T <AD target>\] <srv list> - Assigns a role to one or more targets (users or groups)"
	puts "\t unassignrole \-R <role name> \-T <AD target> \[-T <AD target\] <srv list> - Removes a role from one of more target (users or groups)"
	puts "\t listusers <srv list> - Lists the users visible in the zone"
	puts "\t adduser \[\-U <username>\] \[\-u <UID>\] \[\-g <GID>\] \[\-h <home>\] \[\-s <shell>\] \[\-g <GECOS>\] \-T <AD user>"
	puts "\t rmuser \-T <user name>"
	puts "\t moduser \[\-U <username>\] \[\-u <UID>\] \[\-g <GID>\] \[\-h <home>\] \[\-s <shell>\] \[\-g <GECOS>\] \-T <AD user>"
	puts "\t listgroups <srv list> - Lists the UNIX groups configured in the zone"
	puts "\t addgroup \-G <UNIX group name>  \-T <ad group name>  \[\-g <gid> \] <srv list>"
	puts "\t rmgroup \-T <UNIX group name> <srv list>"
	puts "\t modgroup \[\-G <UNIX group name>\]  \[\-g <gid> \] \-T <ad group name> <srv list>"
	puts "\t listusrADgroups <ad user> - List groups for user"
	puts "\t listADgroup <AD group list> - Lists users in the group"
	puts "\t createADgroup \-t <role|unix|zadmin|zpa> <group name> - Creates a role group, unix placeholder group or a zone admin group"
	puts "\t deleteADgroup <group name> - Deletes an AD group"
	puts "\t addusrADgroup \-g <group name> <user> \[<user>...\]"
	puts "\t rmusrADgroup \-g <group name> <user> \[<user>...\]"
	puts "\t ZPAgroups"
	puts "\t ZAgroups"
	puts "\t ZRolegroups"
	puts "\t ZUnix"
	puts "\t unlock -T <AD SVC Account>"
}

proc doAdPrepare {} {
	global _argv

	set ndx [lsearch $_argv "adprepare"]
	set last [llength $_argv]
	set last [expr {$last - 1}]

	set srvlist ""

	if {($ndx > 0) && ($last > $ndx)} {
		set srvlist [lrange $_argv $ndx+1 $last]
		set srvlist [join $srvlist]
	} else {
		puts stderr "Missing adprepare server argument list"
		exit 1
	}
	if [catch { login } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch {
					createZones $srvlist
				} result ]} {
			global errorInfo
			puts stderr "Error creating zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

proc doPrecreate {} {
	global _argv
	set _zone ""
	set _srvname ""
	
	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp

	if { [getopt _argv -z _zone]} {

	} else {
		usage "Missing precreate target zone"
		exit 1
	}

	if { [getopt _argv -s _srvname]} {
	} else {
		usage "Missing computer name to pre-create"
		exit 1
	}
	
	
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { preCreateComputer $_zone $_srvname } result ]} {
			global errorInfo
			puts stderr "Error preCreateComputer zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

proc doDecomZones {} {
	global _argv

	set ndx [lsearch $_argv "decomzones"]
	set last [llength $_argv]
	set last [expr {$last - 1}]

	set srvlist ""

	if {($ndx > 0) && ($last > $ndx)} {
		set srvlist [lrange $_argv $ndx+1 $last]
		set srvlist [join $srvlist]
	} else {
		puts stderr "Missing decomzones server argument list"
		exit 1
	}
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { decomZones $srvlist } result ]} {
			global errorInfo
			puts stderr "Error removing zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

# end of doDecomZones

proc doActivateZPA {} {
	global _argv

	set ndx [lsearch $_argv "activateZPA"]
	set last [llength $_argv]
	set last [expr {$last - 1}]

	set srvlist "GlobalZone"

	
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { activateZPA $srvlist } result ]} {
			global errorInfo
			puts stderr "Error activatingZPA zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

# end of doActivateZPA procedure

proc doDeactivateZPA {} {
	global _argv
	set ndx [lsearch $_argv "deactivateZPA"]
	set last [llength $_argv]
	set last [expr {$last - 1}]

	set srvlist "GlobalZone"
	
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { deactivateZPA $srvlist } result ]} {
			global errorInfo
			puts stderr "Error deactivatingZPA zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

# end of doDeactivateZPA procedure

proc doListZones { {filter ""}} {
	global _argv
	global domain
	set ndx [lsearch $_argv "listzones"]
	set last [llength $_argv]
	set last [expr {$last - 1}]

	set srvlist ""

	if {($ndx > 0) && ($last > $ndx)} {
		set filter [lrange $_argv $ndx+1 $last]
		set filter [join $filter]
	}

	if [catch { login  } result ] {
		puts stderr "Error logging in to $domain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { listZones $filter } result ]} {
			global errorInfo
			#puts stderr "Error listzones zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			puts stderr $errorInfo
			exit 1
		}
	}
}

# Procedure doListAuth
# Handles the listing of authorizations for a zone
# Authorizations include command to be executed, role definitions and role
# assignments
# The listauth command takes at least 1 parameter
# - the first parameters limits the output to one of the following assignment,
# roles or commands
# - the rest of the parameters constitute the list of zones
proc doListAuth {} {
	global _argv

	set ndx [lsearch $_argv "listauth"]
	set last [llength $_argv]
	set last [expr {$last - 1}]

	set srvlist ""
	set command "all"

	# Handles the case where not argument is specified after the listauth command
	if { $last == $ndx } {
		usage "Missing listauth argument list"
		exit 1
	}

	#Handles the case where there's an argument after the listauth command
	#but no zone names
	#If the argument following the listauth command is not one of the following
	#words "assignment|roles|commands" then the arguments are treated as zone names
	set cmdArg [lindex $_argv $ndx+1]
	set cmdSet 0
	switch -exact $cmdArg {

		"assignment" -
		"roles" -
		"commands" {
			if { $last > $ndx } {
				set cmdSet 1
			} else {
				usage "Missing listauth argument list"
				exit 1
			}
		}
		default { set cmdSet 0}
	}
	#end of switch section

	#Setting the server list and the command filter to
	# be passed to the procedure doing all the work
	if { $cmdSet == 1 } {
		set srvlist [lrange $_argv $ndx+2 $last]
		set srvlist [join $srvlist]
		set command $cmdArg
	} else {
		set srvlist [lrange $_argv $ndx+1 $last]
		set srvlist [join $srvlist]
	}

	if { [string length $srvlist] == 0 } {
		set srvlist "GlobalZone"
	}
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { listauth $command $srvlist } result ]} {
			global errorInfo
			puts stderr "Error listauth zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

# end of doListAuth procedure
#==================================================================================

#==================================================================================
# doCreateCmd procedure
# Adds a new role command to the zone
# The new role command will be named <zone_name>_cmd1,2,3 etc
# The various fields are set by command line parameters
proc doCreateCmd { } {
	global _argv
	set _path "USERPATH"
	set _desc ""
	set _cmd ""
	set _srvlst ""
	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp	
	set Cndx [lsearch $_argv "-C"]
	set Dndx [lsearch $_argv "-D"]
	set Pndx [lsearch $_argv "-P"]
	set argcnt [llength $_argv]
	if { $Cndx == -1 } {
		usage "Missing createcmd command to add"
		exit 1
	} else {
		for {set i $Cndx } {$i < $argcnt} {incr i} {			
			if { $i == $Dndx } {
				set _cmd [lrange $_argv $Cndx+1 $i-1]
			} elseif { $i == $Pndx } {
				set _cmd [lrange $_argv $Cndx+1 $i-1]
			} elseif {$i == $argcnt-1} {
				set _cmd [lrange $_argv $Cndx+1 $i]				
			}
		}
				
	}	
			
	set _srvlst "GlobalZone"
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { createcmd $_cmd $_path $_desc $_srvlst } result ]} {
			global errorInfo
			puts stderr "Error createcmd zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

#End of doCreateCmd
#==================================================================================

#==================================================================================
# doDeleteCmd procedure
proc doDeleteCmd {} {
	global _argv

	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp

	if {[getopt _argv -C _cmd]} {

	} else {
		usage "Missing createcmd command to add"
		exit 1
	}

	checkParams $_argv
	set _srvlst $_argv
	if { [string length $_srvlst] == 0} {
		set _srvlst "GlobalZone"
	}
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { deletecmd $_cmd $_srvlst } result ]} {
			global errorInfo
			puts stderr "Error deletecmd zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

# end of doDeleteCmd
#==================================================================================

#==================================================================================
# doModifyCmd procedure
proc doModifyCmd {} {
	global _argv
	set _path ""
	set _desc ""
	set _cmd ""
	set _cmdalist ""
	set _srvlst ""
	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp

	if {[getopt _argv -A _cmdalias]} {

	} else {
		usage "Missing createcmd command alais to modify"
		exit 1
	}

	getopt _argv -C _cmd

	getopt _argv -D _desc

	getopt _argv -P _path

	checkParams $_argv
	set _srvlst $_argv
	if { [string length $_srvlst] == 0} {
		set _srvlst "GlobalZone"
	}
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { modifycmd $_cmdalias $_cmd $_path $_desc $_srvlst } result ]} {
			global errorInfo
			puts stderr "Error modifycmd zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

#==================================================================================

proc doSearchCmd {} {
	global _argv
	set _cmd ""
	
	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp
	
	if {![getopt _argv -C _cmd]} {
			usage "Missing searchcmd command name"
			exit 1
	}
	
	checkParams $_argv
	
	set _srvlst $_argv
	if { [string length $_srvlst] == 0} {
		set _srvlst "GlobalZone"
	}
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { searchCmd $_cmd $_srvlst } result ]} {
			global errorInfo
			puts stderr "Error searchcmd zones: $result"
			puts stderr "*** Tcl TRACE ***"
			puts stderr $errorInfo
			exit 1
		}
	}
}

#==================================================================================
# procedure doCreateRole
proc doCreateRole {} {
	global _argv

	set _roleName ""
	set _desc "N/A"

	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp

	if {![getopt _argv -R _roleName]} {
		usage "Missing createrole role name"
		exit 1
	}

	getopt _argv -D _desc

	checkParams $_argv

	set _srvlst $_argv
	if { [string length $_srvlst] == 0} {
			set _srvlst "GlobalZone"
	}
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { createrole $_roleName $_desc $_srvlst } result ]} {
			global errorInfo
			puts stderr "Error createrole zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

#==================================================================================

#==================================================================================
# procedure doDeleteRole
proc doDeleteRole {} {
	global _argv

	set _roleName ""

	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp

	if {![getopt _argv -R _roleName]} {
		usage "Missing deleterole role name"
		exit 1
	}

	checkParams $_argv
	set _srvlst $_argv
	if { [string length $_srvlst] == 0} {
			set _srvlst "GlobalZone"
	}
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { deleterole $_roleName $_srvlst } result ]} {
			global errorInfo
			puts stderr "Error deleterole zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

#==================================================================================

proc doAssignCmd {} {
	global _argv

	set _roleName ""
	set _cmdNames ""

	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp

	if {![getopt _argv -R _roleName]} {
		usage "Missing assigncmd role name"
		exit 1
	}

	if {![getopt _argv -C _cmdName]} {
		usage "Missing assigncmd command alias"
		exit 1
	} else {
		lappend _cmdNames $_cmdName

		while {[getopt _argv -C _cmdName]} {
			lappend _cmdNames $_cmdName
		}
	}

	checkParams $_argv
	set _srvlst $_argv
	if { [string length $_srvlst] == 0} {
			set _srvlst "GlobalZone"
	}
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { assigncmd $_roleName $_cmdNames $_srvlst } result ]} {
			global errorInfo
			puts stderr "Error assigncmd zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

#==================================================================================

#==================================================================================
proc doAssignRole {} {
	global _argv
	set _roleName ""
	set _targets ""

	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp

	if {![getopt _argv -R _roleName]} {
		usage "Missing assignrole role name"
		exit 1
	}

	if {![getopt _argv -T _target]} {
		usage "Missing assignrole AD target (group or user)"
	} else {
		lappend _targets $_target
		while {[getopt _argv -T _target]} {
			lappend _targets $_target
		}

	}

	checkParams $_argv
	set _srvlst $_argv
	if { [string length $_srvlst] == 0} {
			set _srvlst "GlobalZone"
	}
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { assignrole $_roleName $_targets $_srvlst } result ]} {
			global errorInfo
			puts stderr "Error assignrole zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

# end of doAssignRole procedure
#==================================================================================

#==================================================================================
proc doUnassignRole {} {
	global _argv
	set _roleName ""
	set _targets ""

	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp

	if {![getopt _argv -R _roleName]} {
		usage "Missing unassignrole role name"
		exit 1
	}

	if {![getopt _argv -T _target]} {
		usage "Missing unassignrole AD target (group or user)"
	} else {
		lappend _targets $_target
		while {[getopt _argv -T _target]} {
			lappend _targets $_target
		}

	}

	checkParams $_argv
	set _srvlst $_argv
	if { [string length $_srvlst] == 0} {
				set _srvlst "GlobalZone"
	}
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { unassignrole $_roleName $_targets $_srvlst } result ]} {
			global errorInfo
			puts stderr "Error unassignrole zones: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

#==================================================================================

#==================================================================================
proc doUserAction { command } {
	global _argv
	global globalZone

	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp

	switch -exact  $command {
		listusers {
			checkParams $_argv
			set _srvlst $globalZone
			if [catch { login  } result ] {
				puts stderr "Error logging in to $addomain with user $aduser: $result"
				exit 1
			} else {
				if {[catch { listusers $_srvlst } result ]} {
					global errorInfo
					puts stderr "Error doUserAction: $result"
					puts stderr "*** Tcl TRACE ***"
					puts stderr $errorInfo
					exit 1
				}
			}

		}

		adduser {
			global _argv
			
			set _usrname ""
			set _uid ""
			set _gid ""
			set _home ""
			set _shell ""
			set _gecos ""
			set _aduser ""

			# Getting rid of the <command> part of _argv
			getopt _argv -c _tmp

			if {![getopt _argv -T _aduser]} {
				usage "Missing AD account argument"
				exit 1
			}

			getopt _argv -U _usrname
			getopt _argv -u _uid
			getopt _argv -g _gid
			getopt _argv -h _home
			getopt _argv -s _shell
			getopt _argv -g _gecos

			checkParams $_argv
			set _srvlst $globalZone
			if [catch { login  } result ] {
				puts stderr "Error logging in to $addomain with user $aduser: $result"
				exit 1
			} else {
				if {[catch { adduser $_usrname $_uid $_gid $_home $_shell $_gecos $_aduser $_srvlst } result ]} {
					global errorInfo
					puts stderr "Error in adduser: $result"
					#puts stderr "*** Tcl TRACE ***"
					#puts stderr $errorInfo
					exit 1
				}
			}
		}

		rmuser {
			global _argv
			set _aduser ""

			# Getting rid of the <command> part of _argv
			getopt _argv -c _tmp

			if {![getopt _argv -T _aduser]} {
				usage "Missing AD account argument"
				exit 1
			}

			checkParams $_argv
			set _srvlst $globalZone

			if [catch { login  } result ] {
				puts stderr "Error logging in to $addomain with user $aduser: $result"
				exit 1
			} else {
				if {[catch { rmuser $_aduser $_srvlst } result ]} {
					global errorInfo
					puts stderr "Error in rmuser: $result"
					#puts stderr "*** Tcl TRACE ***"
					#puts stderr $errorInfo
					exit 1
				}
			}
		}

		moduser {
			global _argv
			set _usrname ""
			set _uid ""
			set _gid ""
			set _home ""
			set _shell ""
			set _gecos ""
			set _aduser ""

			# Getting rid of the <command> part of _argv
			getopt _argv -c _tmp

			if {![getopt _argv -T _aduser]} {
				usage "Missing AD account argument"
				exit 1
			}

			getopt _argv -U _usrname
			getopt _argv -u _uid
			getopt _argv -g _gid
			getopt _argv -h _home
			getopt _argv -s _shell
			getopt _argv -g _gecos

			checkParams $_argv
			set _srvlst $globalZone

			if [catch { login  } result ] {
				puts stderr "Error logging in to $addomain with user $aduser: $result"
				exit 1
			} else {
				if {[catch { moduser $_usrname $_uid $_gid $_home $_shell $_gecos $_aduser $_srvlst } result ]} {
					global errorInfo
					puts stderr "Error in moduser: $result"
					#puts stderr "*** Tcl TRACE ***"
					#puts stderr $errorInfo
					exit 1
				}
			}
		}
		#end moduser

	}

}

#==================================================================================

#==================================================================================
proc doGroupAction {command} {
	global _argv

	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp

	switch -exact  $command {
		listgroups {

			checkParams $_argv
			set _srvlst $_argv
			if [catch { login  } result ] {
				puts stderr "Error logging in to $addomain with user $aduser: $result"
				exit 1
			} else {
				if {[catch { listgroups $_srvlst } result ]} {
					global errorInfo
					puts stderr "Error listgroups: $result"
					#puts stderr "*** Tcl TRACE ***"
					#puts stderr $errorInfo
					exit 1
				}
			}
		}

		addgroup {
			set _adgroup ""
			set _uxgroup ""
			set _gid ""

			if {![getopt _argv -T _adgroup]} {
				usage "Missing AD group argument"
				exit 1
			}

			if {![getopt _argv -G _uxgroup]} {
				usage "Missing AD group argument"
				exit 1
			}

			getopt _argv -g _gid

			checkParams $_argv
			set _srvlst $_argv	
			if {[string length $_srvlst] == 0} {
				set _srvlst "GlobalZone"
			}
			
			if [catch { login  } result ] {
				puts stderr "Error logging in to $addomain with user $aduser: $result"
				exit 1
			} else {
				if {[catch { addgroup $_adgroup $_uxgroup $_gid $_srvlst } result ]} {
					global errorInfo
					puts stderr "Error addgroup: $result"
					#puts stderr "*** Tcl TRACE ***"
					#puts stderr $errorInfo
					exit 1
				}
			}

		}

		rmgroup {
			global _argv
			set _adgroup ""
			

			# Getting rid of the <command> part of _argv
			getopt _argv -c _tmp

			if {![getopt _argv -T _adgroup]} {
				usage "Missing AD group argument"
				exit 1
			}

			checkParams $_argv
			set _srvlst $_argv
			if {[string length $_srvlst] == 0} {
							set _srvlst "GlobalZone"
			}
			if [catch { login  } result ] {
				puts stderr "Error logging in to $addomain with user $aduser: $result"
				exit 1
			} else {
				if {[catch { rmgroup $_adgroup $_srvlst } result ]} {
					global errorInfo
					puts stderr "Error in rmgroup: $result"
					#puts stderr "*** Tcl TRACE ***"
					#puts stderr $errorInfo
					exit 1
				}
			}
		}

		modgroup {
			set _adgroup ""
			set _uxgroup ""
			set _gid ""

			if {![getopt _argv -T _adgroup]} {
				usage "Missing AD group argument"
				exit 1
			}

			getopt _argv -G _uxgroup
			getopt _argv -g _gid

			checkParams $_argv
			set _srvlst $_argv
			if { [string length $_srvlst] == 0} {
				set _srvlst "GlobalZone"
			}
			
			if [catch { login  } result ] {
				puts stderr "Error logging in to $addomain with user $aduser: $result"
				exit 1
			} else {
				if {[catch { modgroup $_adgroup $_uxgroup $_gid $_srvlst } result ]} {
					global errorInfo
					puts stderr "Error addgroup: $result"
					#puts stderr "*** Tcl TRACE ***"
					#puts stderr $errorInfo
					exit 1
				}
			}
		}
	}
	#end switch
}

#end doGroupAction

proc doListUsrADGroups {} {
	global _argv

	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp
	checkParams $_argv

	set adusers $_argv
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { listusrADgroups $adusers } result ]} {
			global errorInfo
			puts stderr "Error listusrADgroups: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

proc doListADGroup {} {
	global _argv

	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp

	set adgroups $_argv

	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { listADgroup $adgroups } result ]} {
			global errorInfo
			puts stderr "Error listADgroup: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

proc doCreateADGroup {} {
	global _argv

	set _type ""
	set _grpName ""

	if {![getopt _argv -t _type]} {
		usage "Missing AD group type"
		exit 1
	}
	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp
	checkParams $_argv
	set _grpName $_argv
	
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { createADGroup $_type $_grpName } result ]} {
			global errorInfo
			puts stderr "Error createADgroup: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

proc doDeleteADGroup {} {
	global _argv
	
	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp
	checkParams $_argv
	set _grpName $_argv
	
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { deleteADgroup $_grpName } result ]} {
			global errorInfo
			puts stderr "Error deleteADgroup: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}
}

proc doAddUserToADGroup {} {
	global _argv
	
	set _grpName ""
	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp
	
	if {![getopt _argv -g _grpName]} {
			usage "Missing AD group name"
			exit 1
	}
	
	checkParams $_argv
	set _usrList $_argv
	
	if [catch { login  } result ] {
			puts stderr "Error logging in to $addomain with user $aduser: $result"
			exit 1
		} else {
			if {[catch { adduserADgroup $_grpName $_usrList} result ]} {
				global errorInfo
				puts stderr "Error adduserADgroup: $result"
				#puts stderr "*** Tcl TRACE ***"
				#puts stderr $errorInfo
				exit 1
			}
		}
}

proc doRmUserFromADGroup {} {
	global _argv
	
	set _grpName ""
	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp
	
	if {![getopt _argv -g _grpName]} {
			usage "Missing AD group name"
			exit 1
	}
	
	checkParams $_argv
	set _usrList $_argv
	
	if [catch { login  } result ] {
			puts stderr "Error logging in to $addomain with user $aduser: $result"
			exit 1
		} else {
			if {[catch { rmusrADgroup $_grpName $_usrList} result ]} {
				global errorInfo
				puts stderr "Error rmusrADgroup: $result"
				#puts stderr "*** Tcl TRACE ***"
				#puts stderr $errorInfo
				exit 1
			}
		}	
}

proc doListUnixAuthGroup {command} {
	if [catch { login  } result ] {
		puts stderr "Error logging in to $addomain with user $aduser: $result"
		exit 1
	} else {
		if {[catch { listUnixAuthGroup $command} result ]} {
			global errorInfo
			puts stderr "Error rmusrADgroup: $result"
			#puts stderr "*** Tcl TRACE ***"
			#puts stderr $errorInfo
			exit 1
		}
	}	
}

proc doUnlock {} {
	global _argv
	
	# Getting rid of the <command> part of _argv
	getopt _argv -c _tmp
	
	if {![getopt _argv -T _svcAcct]} {
				usage "Missing AD Service Account"
				exit 1
	}
	checkParams $_argv
	
	if [catch { login  } result ] {
				puts stderr "Error logging in to $addomain with user $aduser: $result"
				exit 1
			} else {
				if {[catch { unlock $_svcAcct} result ]} {
					global errorInfo
					puts stderr "Error unlock: $result"
					#puts stderr "*** Tcl TRACE ***"
					#puts stderr $errorInfo
					exit 1
			}
	}	
}