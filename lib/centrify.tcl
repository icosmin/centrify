##############################################################
# File: centrify.tcl 
# Purpose: Contains worker procedures which use Centrify's 
#			adedit. All the work happens in this file
# Author: Cosmin IOIART <cosmin.ioiart@bnpparibas.com>
# Copyright: BNP Paribas Arbitrage 
##############################################################


#Establishes user credentials and logs in to AD and
#stores list of zones, domain name, domaindn in global variables
#Input parameters:
#	- AD domain name
#	- user name
#	- path to password file



proc login {{_domain 'gaia.net.intra'} {userid ''} {password ''}} {	
	global env	
	set userid $env(ADUSER)
	set password $env(ADPASSWD)
	set _domain $env(ADDOMAIN)	
	
	if {![info exists env(KRBSSO)]} {		
		if {[catch { bind $_domain $userid $password} result]} {
			global errorInfo
			error $result $errorInfo
		} else {
			global domaindn
			global zonelist
			global domain

			set domaindn [dn_from_domain $_domain]
			set zonelist [get_zones $_domain]
			set domain $_domain
		}
	} else {		
	 	bind $_domain 
		global domaindn
		global zonelist
		global domain
		set domaindn [dn_from_domain $_domain]
		set zonelist [get_zones $_domain]
		set domain $_domain
		
	}
}

# end of login proc
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# Creates a zone under GlobalZone and sets globalzone as its parent
proc createZone {srv} {
	global zoneContainer
	global domaindn
	global domain
	global globalZone
	global zonelist

	#setting DNs for the zone to be created and its parent zone (the Global Zone)
	set zonedn "cn=$srv,cn=$globalZone,$zoneContainer,$domaindn"
	set parentdn "cn=$globalZone,$zoneContainer,$domaindn"

	if {[catch {
				create_zone tree $zonedn
				select_zone $zonedn
				set_zone_field parent $parentdn
				save_zone
				set zonelist [get_zones $domain]
			} result ]} {
		if {[string match *(ENTRY_EXISTS)* $result ]} {
			#ignore already exists exception
		} else {
			global errorInfo
			error $result $errorInfo
		}
	}
	#end of catch phrase for create zone
}

# end of createZone proc

proc preCreateComputer {zone srv} {
	global globalZone
	global zoneContainer
	global domaindn
	global domain
	global hostContainer	
	set srvfqdn [split [exec host $srv]]
	set srvfqdn [lrange $srvfqdn 0 0 ]
	set zonedn "cn=$zone,cn=$globalZone,$zoneContainer,$domaindn"
	
	if {[catch {					
					select_zone $zonedn
					precreate_computer $srv\$@$domain -all  -dnsname $srvfqdn -container $hostContainer
				} result ]} {
			if {[string match *(ENTRY_EXISTS)* $result ]} {
				#ignore already exists exception
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
		#end of catch phrase for create zone
}
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# Creates a ZPA Group for the zone
proc createZPAGroup {srv} {
	global domaindn
	global roleGroupOU

	set zpadn  "cn=$srv-unix-login,$roleGroupOU,$domaindn"

	#Beginning of ZPA Group creation
	if {[catch {
				create_adgroup $zpadn $srv-unix-login  local
			} result ]} {
		if {[string match "*already exists*" $result ]} {
			#ignore already exists exception
		} else {
			global errorInfo
			error $result $errorInfo
		}
	}
	# end of catch phrase for ZPA Group creation
}

# end of createZPAGroup
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# Assigns "UNIX Login" role to the ZPA Group
proc allowLogin {srv} {

	if {[catch {
				assignrole "UNIX Login/GlobalZone" "$srv-unix-login" $srv 1
			} result ]} {
		if {[string match "*already exists*" $result ]} {
			#ignore already exists exception
		} else {
			global errorInfo
			error $result $errorInfo
		}
	}
	# end of catch phrase for ZPA Group creation
}

# end of allowLogin
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# Dispatch procedure for creating a zone ready for action
proc createZones {srvname args} {

	set srvlist [concat $srvname $args]
	
	foreach srv $srvlist {		
		puts "Preparing Centrify AD for \"$srv\" ..."

		if {[catch {
					createZone $srv
					createZPAGroup $srv
					allowLogin $srv					
					preCreateComputer $srv $srv
				} result]} {
			global errorInfo
			error $result $errorInfo
		}
		puts ""
	}
	# end of foreach
}

# end of createZones
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# Configures the zone for ZPA provisioning
# the default username is set to {u:employeeID}
# provisioning_user_unixnameoption set to ZoneDefault
proc decomZones {srvname args} {

	set srvlist [concat $srvname $args]

	foreach srv $srvlist {

		puts -nonewline "Deleting Centrify zone for \"$srv\" ..."
		flush stdout

		if {[catch {
					rmZone $srv
					rmZPAGroup $srv
				} result]} {
			printRed " Fail "
			puts ""
			global errorInfo
			error $result $errorInfo
		}
		printGreen " OK "
		puts ""
		#end of catch phrase
	}
	# end of foreach
}

# end of proc decomzones
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
proc rmZone {srv} {
	global zoneContainer
	global domaindn
	global globalZone

	#setting DNs for the zone to be deleted
	set zonedn "cn=$srv,cn=$globalZone,$zoneContainer,$domaindn"
	if {[catch {
				select_zone $zonedn
				delete_zone
			} result ]} {
		if {[string match *(NO_OBJECT)* $result ]} {

		} else {
			global errorInfo
			error $result $errorInfo
		}
	}
}

# end rmZone
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# Removes the ZPA Group
proc rmZPAGroup {srv} {
	global domaindn
	global roleGroupOU

	set zpadn  "cn=$srv-unix-login,$roleGroupOU,$domaindn"

	#Beginning of ZPA Group creation
	if {[catch {
				select_object $zpadn
				delete_object
			} result ]} {
		if {[string match *(NO_OBJECT)* $result ]} {
			#ignore already exists exception
		} else {
			global errorInfo
			error $result $errorInfo
		}
	}
	# end of cathc phrase for ZPA Group deletion
}


#----------------------------------------------------------------------------------
proc addZPAConf {srv} {
       global zonelist
       global domain
       global globalZone

       set srv $globalZone
       #setting DNs for the zone to be deleted
       set zonedn [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
       if { [string length $zonedn ] == 0 } {
               puts "$srv\tdoes not exist\n"
               exit 1
       }
       if {[catch {
                               select_object $zonedn
                               set zoneDescription [get_object_field "description"]

                               #remove "provisioning_" entries
                               set ndxList ""
                               set ndxList [lsearch -all  -regexp $zoneDescription "provisioning_.*\|truncatename:.*\|username:.*"]
                               foreach ndx $ndxList {
                                       set desc [lindex $zoneDescription $ndx]
                                       remove_object_value $zonedn "description" "$desc"
                               }

                               #add provisioning_ entries
                               add_object_value $zonedn "description" "truncatename:true"
                               add_object_value $zonedn "description" "username:\$\{u:employeeID\}"
										 add_object_value $zonedn "description" "provisioning_custom_uid_attribute:employeeId"
                               add_object_value $zonedn "description" "provisioning_unixname_truncate_option:FirstEightCharacter"
                               add_object_value $zonedn "description" "provisioning_unixname_prefix:l"
                               add_object_value $zonedn "description" "provisioning_user_enabled:True"
                               add_object_value $zonedn "description" "provisioning_gidoption:Auto"
                               add_object_value $zonedn "description" "provisioning_uidoption:CustomId"
                               add_object_value $zonedn "description" "provisioning_groupsource:"
                               add_object_value $zonedn "description" "provisioning_group_enabled:False"
                               add_object_value $zonedn "description" "provisioning_usersource:Global-UNIX-Accounts@gaia.net.intra"
                               add_object_value $zonedn "description" "provisioning_gecos_option:ZoneDefault"
                               add_object_value $zonedn "description" "provisioning_homedirectory_explicit:%{home}/%{user}"
                               add_object_value $zonedn "description" "provisioning_homedirectory_option:ZoneDefault"
                               add_object_value $zonedn "description" "provisioning_shell_explicit:%{shell}"
                               add_object_value $zonedn "description" "provisioning_shell_option:ZoneDefault"
                               add_object_value $zonedn "description" "provisioning_unixname_invalid_character_option:ReplaceByUnderscore"
                               add_object_value $zonedn "description" "provisioning_unixname_lower_case:True"
                               add_object_value $zonedn "description" "provisioning_unixname_valid_characters:abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_"
                               add_object_value $zonedn "description" "provisioning_group_unixnameoption:SamAccountName"
                               add_object_value $zonedn "description" "provisioning_primarygid_option:Explicit"
										 add_object_value $zonedn "description" "provisioning_primarygid_explicit:6000"
                               add_object_value $zonedn "description" "provisioning_user_unixnameoption:ZoneDefault"
                       } result ]} {
               global errorInfo
               error $result $errorInfo
       }
       # end of catch phrase
}

#end of addZPAConf
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# Enables the ZPA
proc activateZPA {srvname args} {

       set srvlist [concat $srvname $args]
       
       foreach srv $srvlist {

               puts -nonewline "Activating ZPA for \"$srv\" ..."
               flush stdout

               if {[catch {
                                       addZPAConf $srv
                               } result]} {
                       printRed " Fail "
                       puts ""
                       global errorInfo
                       error $result $errorInfo
               }
               printGreen " OK "
               puts ""
               #end of catch phrase
       }
       # end of foreach
}

#end of activateZPA procedure
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# Disables the ZPA
proc deactivateZPA {srvname args} {
	global zonelist
	
	set srvlist [concat $srvname $args]

	foreach srv $srvlist {

		global zoneContainer
		global domaindn
		global globalZone

		#setting DNs for the zone to be deleted		
		set zonedn [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		puts -nonewline "Deactivating ZPA for \"$srv\" ..."
		flush stdout
		if {[catch {
					select_object $zonedn
					set zoneDescription [get_object_field "description"]

					#remove "provisioning_" entries
					set ndxList [lsearch -all  $zoneDescription "provisioning_user_enabled*"]
					foreach ndx $ndxList {
						set desc [lindex $zoneDescription $ndx]
						remove_object_value $zonedn "description" "$desc"
					}
					add_object_value $zonedn "description" "provisioning_user_enabled:False"
					save_object
				} result]} {
			printRed " Fail "
			puts ""
			global errorInfo
			error $result $errorInfo
		}
		printGreen " OK "
		puts ""
		#end of catch phrase
	}
	# end of foreach
}

# end of deactivateZPA procedure
#----------------------------------------------------------------------------------
proc ZPAStatus { zone } {
	global errorInfo
	push
	select_object $zone
	set zoneDescription [get_object_field "description"]
	pop
	set ndxList [lsearch -all  $zoneDescription "provisioning_user_enabled:True"]
	if { $ndxList > -1 } {
		return "True"
	} else {
		return "False"
	}
}

#----------------------------------------------------------------------------------
proc listZones {filter} {
	global zonelist

	if {[catch {
				set zoneTree {}

				foreach _zone $zonelist {
					select_zone $_zone
					set _parent [get_zone_field "parent"]
					set _parent [extractZoneFromDN $_parent]
					set _ndx [search $zoneTree $_parent]
					if { $_ndx == -1 } {
						lappend zoneTree [extractZoneFromDN $_zone]
					} else {
						set zoneTree [addSubtree $zoneTree $_ndx [extractZoneFromDN $_zone]]
					}

				}
				foreach i [traverse $zoneTree] {
					if { [string length $filter] > 0} {
						set _apath [absolutePath $zoneTree $i]
						set lastndx [expr [llength $_apath]-1]
						if {[string match -nocase "*$filter*" [lindex $_apath $lastndx]]} {
							puts [join [absolutePath $zoneTree $i] "\->"]
						}
					} else {
						puts [join [absolutePath $zoneTree $i] "\->"]
					}
				}
			} result]} {
		global errorInfo
		error $result $errorInfo
	}
	#end of catch phrase
}

# end of listZones procedure
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# Section handling listauth functionality
proc listauth_assignment {srv _zone} {
	global domaindn

	printTitle "$srv Role assignments"
	if {[catch {
				select_zone $_zone
				set role_assignments [get_role_assignments -upn]
				foreach role_assign $role_assignments {
					set role_assign [split $role_assign "/"]

					# Extracting name of user/group for the role assignment target
					set roleTgt [lindex $role_assign 0]
					set roleTgt [split $roleTgt "@"]
					set roleTgt [lindex $roleTgt 0]
					# Searching for AD objects containing the sAMAccountName we want
					set srcObjects [ get_objects -depth sub "$domaindn" (sAMAccountName=$roleTgt)]
					# Displaying "Group" or "User" next to the assignment target. From where we stand
					# it is impossible
					# to tell if the target is a group or a user account.
					if {[llength $srcObjects] > 0 } {
						select_object -attrs objectClass [lindex $srcObjects 0]
						set objClass [get_object_field objectClass]
						if { [lsearch $objClass "group"] != -1} {
							puts "\t(Group)$roleTgt => [lindex $role_assign 1]/[lindex $role_assign 2]"
						} elseif { ([lsearch $objClass "person"] != -1) || ([lsearch $objClass "User"] != -1) } {
							puts "\t(User)$roleTgt => [lindex $role_assign 1]/[lindex $role_assign 2]"
						}
					} else {
						# In case the fields are impossible to detect, the question mark tells us that
						# the script has no idea what type of object it's dealing with
						puts "\t       [lindex $role_assign 0] => [lindex $role_assign 1]/[lindex $role_assign 2]"
					}
				}
				puts ""
			} result]} {
		global errorInfo
		error $result $errorInfo
	}
}

#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
proc listauth_roles {srv _zone} {
	printTitle "$srv Role definitions"
	if {[catch {
				select_zone $_zone
				set roleList [get_roles]
				if { [llength $roleList] == 0 } {
					puts "\t--- none ---"
				}

				foreach role $roleList {
					select_role $role
					puts "\t$role => \[ [get_role_field description] \]"
					set role_cmds [get_role_commands]
					foreach role_cmd $role_cmds {
						puts "\t\t- $role_cmd"
					}
					set role_apps [get_role_apps]
					foreach role_app $role_apps {
						puts "\t\t- $role_app"
					}
					puts ""
				}
				puts ""
			} result]} {
		global errorInfo
		error $result $errorInfo
	}

}

#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
proc listauth_commands { srv _zone} {
	printTitle "$srv DZDO (sudo) commands & PAM apps"
	#For each zone specified, the script iterates through all the
	#dzdo commands and displays the command line, runas user, path and description
	# in case of error the exception is rethrown
	if {[catch {
				select_zone $_zone
				set dz_commands [get_dz_commands]

				foreach dz_cmd $dz_commands {
					select_dz_command $dz_cmd
					puts "\t$dz_cmd: CMD=[get_dzc_field cmd] DZDO_RUNAS=[get_dzc_field dzdo_runas] \
				DZSH_RUNAS=[get_dzc_field dzsh_runas] PATH=[get_dzc_field path] \[ [get_dzc_field description] \]"
				}
				set pam_apps [get_pam_apps]
				foreach pam_app $pam_apps {
					select_pam_app $pam_app
					puts "\t$pam_app: APP=[get_pam_field application] \[ [get_pam_field description] \]"
				}
				puts ""
			} result ]} {
		global errorInfo
		error $result $errorInfo
	}
}

#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
proc listauth { command srvlist} {
	global zonelist
	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]

		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}

		if {[catch {
					if { $command == "all" } {
						listauth_commands $srv $_zone
						listauth_roles $srv $_zone
						listauth_assignment $srv $_zone
					} else {
						listauth_$command $srv $_zone
					}
				} result ]} {
			global errorInfo
			error $result $errorInfo
		}
	}
}

# End of section handling listauth functionality
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
proc createcmd {cmd path desc srvlist} {
	global zonelist	
	foreach srv $srvlist {		
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]		
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		puts "Adding dzdo (sudo) command $cmd to zone $srv"
		if {[string length [searchCmd $cmd $srv 1]] > 0 } {
				continue;		}
		if {[catch {					
					select_zone $_zone
					set dzCmds [get_dz_commands]
					set dzCmdsCnt [llength $dzCmds]					
					set cmdName "cmd_$srv$dzCmdsCnt"
					new_dz_command $cmdName
					set_dzc_field cmd $cmd
					set_dzc_field form "1"
					set_dzc_field path $path			
					set_dzc_field description $desc
					set_dzc_field dzdo_runas "*:*"
					set_dzc_field dzsh_runas "$"
					set_dzc_field flags 16			      
					save_dz_command					
				} result ] } {
			global errorInfo
			error $result $errorInfo
		}

	}
}

#End of createcmd
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
proc deletecmd {cmd srvlist} {
	global zonelist

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		puts "Deleting dzdo (sudo) command $cmd from zone $srv"
		if {[catch {
					select_zone $_zone
					select_dz_command $cmd
					delete_dz_command
				} result ] } {
			if {[string match "*right not found*" $result ]} {
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}

	}
}

#End of createcmd
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
proc modifycmd {cmdalias cmd path desc srvlist} {
	global zonelist

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		puts "Modifying dzdo (sudo) command $cmd for zone $srv"
		if {[catch {
					select_zone $_zone
					select_dz_command $cmdalias
					if {[string length $cmd] > 0} {
						set_dzc_field cmd $cmd
					}

					if {[string length $path] >0} {
						set_dzc_field path $path
					}

					if {[string length $desc] > 0} {
						set_dzc_field description $desc
					}
					save_dz_command
				} result ] } {
			if {[string match "*right not found*" $result ]} {
				puts stderr "\tCommand alias $cmdalias/$srv does not exist"
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}

	}
}

# end of modifycmd
#----------------------------------------------------------------------------------

proc searchCmd {cmd srvlist {silent 0}} {
	global zonelist
	
	foreach srv $srvlist {
		set retvalue ""
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}		
		if {[catch {
					select_zone $_zone
					set dzCommands [get_dz_commands]
					foreach _dzCmd $dzCommands {
						select_dz_command $_dzCmd						
						set dzcmdName [get_dzc_field cmd]
						#puts "dzcmdName=$dzcmdName"
						if { $dzcmdName == $cmd } {
							if { $silent == 0 } {
								puts "$_dzCmd: $dzcmdName"
							} else {
								set retvalue $_dzCmd
							}
						}
					}					
		} result ] } {
			if {[string match "*right not found*" $result ]} {
				puts stderr "\tCommand alias $cmdalias/$srv does not exist"
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
		if {[string length $retvalue] > 0 } {			
			return $retvalue
		}
	}
}
#----------------------------------------------------------------------------------
proc createrole {roleName desc srvlist} {
	global zonelist

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		puts "Adding role $roleName to zone $srv"
		if {[catch {
					select_zone $_zone
					new_role $roleName
					set_role_field description $desc
					set_role_field sysrights 15
					set_role_field timebox "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
					save_role
				} result ] } {
			if {[string match "*Role already in zone*" $result ]} {
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}

	}
}

# end of createrole procedure
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
proc deleterole {roleName srvlist} {
	global zonelist

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		puts "Deleting role $roleName from zone $srv"
		if {[catch {
					select_zone $_zone
					select_role $roleName
					delete_role
				} result ] } {
			if {[string match "*(NO_OBJECT)*" $result ]} {
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}

	}
}

# end of createrole procedure
#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
proc assigncmd {roleName cmdNames srvlist} {
	global zonelist

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		puts "Assigning commands \[$cmdNames\] to role $roleName in zone $srv"
		if {[catch {
					select_zone $_zone
					set dzCmds [get_dz_commands]
					set pamApps [get_pam_apps]
					select_role $roleName

					foreach cmdName $cmdNames {
						set _tmpName [lsearch -inline -exact $dzCmds $cmdName]
						if {[string length $_tmpName] > 0} {
							if {[catch {
										add_command_to_role $_tmpName
									} result ]} {
								if {[string match "*Right already in role*" $result ]} {
									continue
								} else {
									global errorInfo
									error $result $errorInfo
								}
							}
						} else {
							set _tmpName [lsearch -inline -exact $pamApps $cmdName]
							if {[string length $_tmpName] > 0} {
								if {[catch {
											add_pamapp_to_role $_tmpName
										} result ]} {
									if {[string match "*Right already in role*" $result ]} {
										continue
									} else {
										global errorInfo
										error $result $errorInfo
									}
								}
							} else {
								puts stderr "\t Command $cmdName not found"
							}
						}
					}
					save_role
				} result ] } {
			if {[string match "*Right already in role*" $result ]} {
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
	}
}

#-----------------------------------------------------------------

#----------------------------------------------------------------------------------
proc assignrole {role targets srvlist {silent 0}} {
	global zonelist
	global domain

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}

		if { $silent == 0 } {
			puts "Assigning role $role to $targets for zone $srv"
		}
		if {[catch {
					select_zone $_zone
					#Assigning role to all targets
					foreach target $targets {
						if {[catch {
									new_role_assignment "$target@$domain"
									set_role_assignment_field role $role
									save_role_assignment
								} result]} {
							if {[string match "*Role assignment already exists*" $result ]} {
								continue
							} else {
								global errorInfo
								error $result $errorInfo
							}
						}
						#end of new_role catch block
					}
				} result]} {
			global errorInfo
			error $result $errorInfo
		}
		#end if-catch block
	}
	#end of foreach srv loop
}

#end of assignrole procedure
#-----------------------------------------------------------------

#-----------------------------------------------------------------
proc unassignrole {role targets srvlist} {
	global zonelist
	global domain

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		puts "Unassigning role $role from $targets for zone $srv"
		if {[catch {
					select_zone $_zone
					#Assigning role to all targets
					foreach target $targets {
						if {[catch {
									select_role_assignment "$target@$domain/$role"
									delete_role_assignment
								} result]} {
							if {[string match "*Role assignment already exists*" $result ]} {
								continue
							} else {
								global errorInfo
								error $result $errorInfo
							}
						}
						#end of new_role catch block
					}
				} result]} {
			global errorInfo
			error $result $errorInfo
		}
		#end if-catch block
	}
	#end of foreach srv loop
}

#-----------------------------------------------------------------

#-----------------------------------------------------------------
proc listusers { srvlist} {
	global zonelist
	global domaindn

	
	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		puts ""
		printTitle "User list for $srv"
		if {[catch {
					select_zone $_zone
					set _zoneUsers [get_all_zone_users $_zone]
					foreach _usr $_zoneUsers {
						select_zone_user $_usr
						set _samname [lindex [split $_usr "@"] 0]

						set _adusr [get_objects -depth sub -limit 1 "$domaindn" (samaccountname=$_samname)]
						if {[catch {
							select_object -attrs "displayName" [lindex $_adusr 0]
						} result ]} {
							continue;
						}
						set _dispName [lindex [get_object_field "displayName"] 0]
						puts "[gzuf uname]:[gzuf uid]:[gzuf gid]:$_usr \[$_dispName\]"

					}
				} result ]} {
			if {[string match "*User not in current zone*" $result ]} {
				continue
			} else {

				global errorInfo
				error $result $errorInfo
			}
		}

	}
}

#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
# Adds a new user to a zone
proc adduser {_usrname _uid _gid _home _shell _gecos _aduser srvlist {silent 0} } {
	global zonelist	
	global domain
	global domaindn

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		
		if { [ZPAStatus $_zone] == "True" } {
			puts stderr "ZPA is enabled for $srv. Manual user provisioning is disabled"
			continue
		}
		# Either the name of the zone is metazone or there is no metazone and we are
		# adding
		# the account to the zone
		# No metazone exists, we are adding the account directly into the account
		if { $silent == 0 } {
			puts "Creating UNIX profile for user $_aduser on $srv"
		}

		if {[catch {

					select_zone $_zone

					set _zoneUsers [get_zone_users]
					if { [lsearch $_zoneUsers "$_aduser@$domain"] != -1 } {
						select_zone_user $_aduser@$domain
						if { 	(([string length $_usrname] > 0) && ($_usrname != [gzuf "uname"])) ||
							(([string length $_uid] > 0) && ($_uid != [gzuf "uid"])) ||
							(([string length $_gid] > 0) &&($_gid != [gzuf "gid"])) ||
							(([string length $_home] > 0) && ($_home != [gzuf "home"])) ||
							(([string length $_shell] > 0) &&($_shell != [gzuf "shell"])) ||
							(([string length $_gecos] > 0) &&($_gecos != [gzuf "gecos"]))   } {
							# The user already exists and has different fields
							puts "User already exists in zone $srv with different fields"
							exit 1

						}
						# The user already exists for the zone
						error "Duplicate entry"
					}
					new_zone_user $_aduser@$domain
					set _adusrDN [get_objects -depth sub  "$domaindn" (samaccountname=$_aduser)]
					select_object [lindex $_adusrDN 0]
					set _dispName [lindex [get_object_field "displayName"] 0]
					set _sid [lindex [get_object_field "sid"] 0]
					set _acctname [lindex [get_object_field "sAMAccountName"] 0]

					if {[string length $_usrname] > 0} {
						szuf "uname" $_usrname
					} else {
						szuf "uname" $_acctname
					}

					if {[string length $_uid] > 0} {
						push
						#checking that uid does not already exist
						set _zusersuid ""
						set _allzusers [get_zone_users]
						foreach _zuser $_allzusers {
							select_zone_user $_zuser
							lappend _zusersuid [gzuf "uid"]
						}
						pop
						if { [lsearch $_zusersuid $_uid] > -1 } {
							error "UID in use"
						} else {
							szuf "uid" $_uid
						}
					} else {
						szuf "uid" [sid_to_uid $_sid]
					}

					if {[string length $_gid] > 0} {
						szuf "gid" $_gid
					} else {
						szuf "gid" "0x80000000"
					}

					if {[string length $_gecos] > 0} {
						szuf "gecos" $_gecos
					} else {
						szuf "gecos" "%{u:displayName}"
					}

					if {[string length $_home] > 0} {
						szuf "home" $_home
					} else {
						szuf "home" "%{home}/%{user}"
					}

					if {[string length $_shell] > 0} {
						szuf "shell" $_shell
					} else {
						szuf "shell" "%{shell}"
					}
					save_zone_user
					puts "User [gzuf uname] has been created on $srv"
				} result ]} {
			if { [string match "*UID in use*" $result] } {
				if { $silent == 0 } {
					puts "The UID $_uid is already used by another group on $srv"
				}
				if { $srv != $metaZone } {
					continue
				} else {
					puts "The UID $_uid is already used by another group on $srv"
					exit 1
				}
			} elseif { [string match "*Duplicate entry*" $result] } {
				if { $silent == 0 } {
					puts "$_aduser@$domain already exists in the zone"
				}
				continue
			} elseif { [string match "*Unix name*" $result] } {
				if { $silent == 0 } {
					puts stderr "Account [gzuf uname] already exists in zone"
				}
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
		#end catch

	}
}

#----------------------------------------------------------------------------------

#----------------------------------------------------------------------------------
proc rmuser { username srvlist } {
	global zonelist
	global domain

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}

		if { [ZPAStatus $_zone] == "True" } {
			puts stderr "ZPA is enabled for $srv. Manual user deprovisioning is disabled. Remove user from ZPA group"
			continue
		}

		if {[catch {
					select_zone $_zone
					select_zone_user $username@$domain

					delete_zone_user
				} result]} {
			if { [string match "*User not in current zone*" $result] } {
				puts "User $username not in zone $srv"
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
		#end catch block
	}
	# end foreach
}

#end rmuser

#----------------------------------------------------------------------------------
proc moduser {usrname uid gid home shell gecos aduser srvlist  } {
	global zonelist	
	global domaindn

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		puts "Modifying user $aduser on $srv..."
		if {[catch {
					select_zone $_zone

					select_zone_user [domainFix $aduser]
					# checking for duplicate UID
					if { $uid == "sid" } {
						set _adusrDN [get_objects -depth sub  "$domaindn" (samaccountname=$aduser)]
						select_object [lindex $_adusrDN 0]
						set _sid [lindex [get_object_field "sid"] 0]
						szuf "uid" [sid_to_uid $_sid]
					} elseif {[string length $uid] > 0} {
						push
						#checking that uid does not already exist
						set _zusersuid ""
						set _allzusers [get_zone_users]
						foreach _zuser $_allzusers {
							select_zone_user $_zuser
							lappend _zusersuid [gzuf "uid"]
						}
						pop
						if { [lsearch $_zusersuid $uid] > -1 } {
							error "Duplicate UID"
						} else {
							szuf "uid" $uid
						}
					}
					#end if
					if {[string length $usrname ] > 0} {
						push
						#checking that username does not already exist
						set _allzusers [get_zone_users]
						set _zusernames ""
						foreach _zuser $_allzusers {
							select_zone_user $_zuser
							lappend _allzusers [gzuf "uname"]
						}
						pop
						if { [lsearch $_allzusers $usrname] > -1 } {
							error "Duplicate user name"
						} else {
							szuf "uname" $usrname
						}
					}

					if {[string length $gid ] > 0} {
						if { $gid == "private" } {
							szuf gid "0x80000000"
						} else {
							szuf "gid" $gid
						}
					}

					if {[string length $home ] > 0} {
						szuf "home" $home
					}

					if {[string length $shell ] > 0} {
						szuf "shell" $shell
					}

					if {[string length $gecos ] > 0} {
						szuf "gecos" $gecos
					}
					save_zone_user
				} result]} {
			if { [string match "*Duplicate UID*" $result] } {
				puts "UID $uid already exists in the zone"
			} elseif { [string match "*Duplicate user name*" $result] } {
				puts "User $usrname already exists in the zone"
				continue
			} elseif { [string match "*User not in current zone*" $result] } {
				puts "User $usrname not in zone $srv"
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
		#end catch block
	}
	#end foreach

}

# end moduser

#----------------------------------------------------------------------------------
proc listgroups { srvlist } {
	global zonelist
	global domaindn

	if {[lsearch srvlist "GlobalZone"] == -1} {
		lappend srvlist "GlobalZone"
	}

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		puts ""
		printTitle "UNIX Groups for $srv"
		if {[catch {
					select_zone $_zone
					list_zone_groups
				} result ]} {
			if {[string match "*User not in current zone*" $result ]} {
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
		#end catch block
	}
	#end foreach
}

#end listgroups

#----------------------------------------------------------------------------------
proc addgroup { adgroup uxgroup gid srvlist {silent 0}} {
	global zonelist
	global metaZone
	global domain
	global domaindn

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}

		if { $silent == 0 } {
			puts "Creating UNIX group for $adgroup on $srv"
		}

		if {[catch {
					select_zone $_zone
					set _zoneGroups [get_zone_groups]

					set _adusrDN [get_objects -depth sub  "$domaindn" (samaccountname=$adgroup)]
					select_object [lindex $_adusrDN 0]
					set _sid [lindex [get_object_field "sid"] 0]

					if {[string length $gid] == 0} {
						set gid [sid_to_uid $_sid]
					}

					if { [lsearch -regex $_zoneGroups "^$adgroup@"] != -1 } {
						#The AD group already has an entry in the zone
						# checking if the UNIX group name and GID are the same
						select_zone_group $adgroup@$domain
						if { (([string length $uxgroup] > 0 ) && ([gzgf "name"] != $uxgroup)) ||
							(([string length $gid] > 0) && ([gzgf "gid"] != $gid)) } {
							# The group exists with different fields
							puts stderr "Group $adgroup alread exists with different field values"
							exit 1
						}
						error "Group already defined"
					} else {
						new_zone_group $adgroup@$domain

						szgf "name" $uxgroup

						if {[string length $gid] > 0} {
							push
							#checking that uid does not already exist
							set _zgroupids ""
							set _zallgroups [get_zone_groups]
							foreach _zgroup $_zallgroups {
								select_zone_group $_zgroup
								lappend _zgroupids [gzgf "gid"]
							}
							pop
							if { [lsearch $_zgroupids $gid] != -1 } {
								if { $silent == 0 } {
									puts stderr "GID $gid is already in use"
								}
								error "GID in use"
							} else {
								szgf "gid" $gid
							}
						}
						save_zone_group
						puts "UNIX Group [gzgf name] has been created on $srv"
					}
				} result]} {
			if { [string match "*GID in use*" $result] } {
				if { $silent == 0 } {
					puts "The GID $gid is already used by another group on $srv"
				}
				if { $srv != $metaZone } {
					continue
				} else {
					puts "The GID $gid is already used by another group on $srv"
					exit 1
				}
			} elseif { [string match "*Group already defined*" $result] } {
				if { $silent == 0 } {
					puts "Group $adgroup already defined on $srv"
					continue
				}
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
		#end catch block
	}
	#end foreach
}

#end addgroup

#----------------------------------------------------------------------------------
proc rmgroup {adgroup srvlist} {
	global zonelist
	global domain

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}
		puts "Removing group $adgroup from $srv"
		if {[catch {
					select_zone $_zone
					select_zone_group $adgroup@$domain

					delete_zone_group
				} result]} {
			if { [string match "*could not find zone entry*" $result] } {
				puts "Group $adgroup not in zone $srv"
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
		#end catch block
	}
	# end foreach
}

#end rmgroup

#----------------------------------------------------------------------------------
proc modgroup { adgroup uxgroup gid srvlist } {
	global zonelist
	global domain
	global domaindn

	foreach srv $srvlist {
		#seraches for the zone's DN in the zone list and displays a message
		#if the zone is not found. The search is case sensitive
		set _zone [lsearch -regexp -inline $zonelist "CN=$srv,.*"]
		if { [string length $_zone ] == 0 } {
			puts "$srv\tdoes not exist\n"
			continue
		}

		puts "Modifying UNIX group for $adgroup on $srv"

		if {[catch {
					select_zone $_zone

					set _adusrDN [get_objects -depth sub  "$domaindn" (samaccountname=$adgroup)]
					select_object [lindex $_adusrDN 0]
					set _sid [lindex [get_object_field "sid"] 0]

					select_zone_group $adgroup@$domain

					if {[string length $uxgroup] > 0 } {
						szgf "name" $uxgroup
					}

					if { $gid == "sid" } {
						set gid [sid_to_uid $_sid]
					} elseif {[string length $gid] > 0} {
						push
						#checking that uid does not already exist
						set _zgroupids ""
						set _zallgroups [get_zone_groups]
						foreach _zgroup $_zallgroups {
							select_zone_group $_zgroup
							lappend _zgroupids [gzgf "gid"]
						}
						pop
						if { [lsearch $_zgroupids $gid] != -1 } {
							puts stderr "GID $gid is already in use"
							error "GID in use"
						} else {
							szgf "gid" $gid
						}
					}
					save_zone_group
				} result]} {
			if { [string match "*Duplicate Unix name*" $result] } {
				puts "The UNIX group name $uxgroup is already in use on $srv"
				continue
			} elseif { [string match "*GID in use*" $result] } {
				puts "The GID $gid is already used by another group on $srv"
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
		#end catch block
	}
	#end foreach
}

proc listusrADgroups { adusers } {
	if {[catch {
				foreach aduser $adusers {
					set aduser [domainFix $aduser]
					printTitle "$aduser Groups"
					set usergrps [get_user_groups $aduser]
					if { [llength $usergrps ] == 0 } {
						puts "\tN/A"
					} else {
						foreach usrgrp $usergrps {
							puts "\t$usrgrp"
						}
					}
				}
			} result] } {
		global errorInfo
		error $result $errorInfo
	}
}

proc listADgroup { adgroups } {
	global domain
	puts "AD groups = $adgroups"
	
	if {[catch {	
					set adgrp $adgroups
					set adgrp [domainFix $adgrp]
					printTitle "$adgrp User List"
					set grpusers [get_group_members $adgrp]
					if { [llength $grpusers ] == 0 } {
						puts "\tN/A"
					} else {
						foreach grpusr $grpusers {
							set usrDomDN [getDomDN $grpusr]
							set _usrDN [get_objects -depth sub  "$usrDomDN" (samaccountname=[getSAMAcctName $grpusr])]
							set _usrDN [lindex $_usrDN 0]
							select_object "$_usrDN"
							set dispName [join [get_object_field "displayname"]]
							set sAMAcct [get_object_field ""]
							puts "\t$grpusr \[$dispName\]"
						}
					}				
			} result] } {
		global errorInfo
		error $result $errorInfo
	}
}

proc createADGroup { type grpName } {
	global domaindn
	global roleGroupOU
	global unixGroupOU
	global ZAGroupOU
	global zpaContainer
	
	
	set grpName [getSAMAcctName $grpName]
	set targetOU ""
	switch -exact $type {
		role {
			set targetOU "CN=$grpName,$roleGroupOU,$domaindn"
		}
		unix {
			set targetOU "CN=$grpName,$unixGroupOU,$domaindn"
		}
		zadmin {
			set targetOU "CN=$grpName,$ZAGroupOU,$domaindn"
		}
		
		zpa {
			set targetOU "CN=$grpName,$zpaContainer,$domaindn"
		}
	}
	
	if {[catch {
				create_adgroup "$targetOU" $grpName "local"
			} result] } {
		if {[string match "*Object already exists*" $result ]} {
			#ignore already exists exception
		} else {
			global errorInfo
			error $result $errorInfo
		}
	}
}



proc deleteADgroup { grpName } {	
	if {[catch {
		foreach _grp $grpName {
			set _grp [domainFix $_grp]
			set _grpSAM [getSAMAcctName $_grp]
			set domDN [getDomDN $_grp]
			
			regsub -all {\s} $_grpSAM {\0} _grpSAM  
			
			set _groupDN [lindex [get_objects -depth sub -limit 10 "$domDN" (samaccountname=$_grpSAM)] 0]
			
			if { [string length $_groupDN] > 0} { 
				select_object $_groupDN
				delete_object
			}
		}		
	} result ]} {
		global errorInfo
		error $result $errorInfo
	}	
}

proc adduserADgroup { grpName usrList } {
	set grpName [domainFix $grpName]
	foreach _usr $usrList {
		if {[catch {		
					set _usr [domainFix $_usr]
					add_user_to_group $_usr $grpName
		} result ]} {
			if {[string match "*(ENTRY_EXISTS)*" $result ]} {
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
	}
}

proc rmusrADgroup { grpName usrList } {
	set grpName [domainFix $grpName]
	foreach _usr $usrList {
		if {[catch {		
					set _usr [domainFix $_usr]					
					remove_user_from_group $_usr $grpName
		} result ]} {
			if {[string match "*(WILL_NOT_PERFORM)*" $result ]} {
				continue
			} else {
				global errorInfo
				error $result $errorInfo
			}
		}
	}
}

proc listUnixAuthGroup { command } {
	global domaindn
	global roleGroupOU
	global unixGroupOU
	global ZAGroupOU
	global zpaContainer
	switch -exact $command {
			ZRolegroups {
				set targetOU "$roleGroupOU,$domaindn"
			}
			ZUnix {
				set targetOU "$unixGroupOU,$domaindn"
			}
			ZAgroups {
				set targetOU "$ZAGroupOU,$domaindn"
			}
			
			ZPAgroups {
				set targetOU "$zpaContainer,$domaindn"
			}
		}
	set objList [go -depth sub $targetOU (objectclass=group)]
	foreach _obj $objList {
		set _objDom [domain_from_dn $_obj]
		select_object $_obj
		set _objAMA [get_object_field "sAMAccountName" ]
		puts "$_objAMA@$_objDom"		
		
	}
}

proc unlock {svcAcct} {
	global svcAcctContainer
	global domaindn
	
	if {[catch {
					
			set svcAcct [domainFix $svcAcct]
			set svcAcctDN [get_objects -limit 1 "$svcAcctContainer,$domaindn" (userPrincipalName=$svcAcct)]
			if {[llength $svcAcctDN] == 0} {
				puts "Not found"	
				exit 0
			}
		
			select_object [lindex $svcAcctDN 0]
			set_object_field "lockoutTime" 0
			save_object

	} result] } {
			global errorInfo
			error $result $errorInfo
	}
}