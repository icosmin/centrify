#!/bin/bash

TGT=$1

CENTRIFY="/bnp/files/Centrify"

CENTRIFY_VER="5.1.1-831"

OS=$(ssh -q root@$TGT 'uname -s')
REL=$(ssh -q root@$TGT 'uname -r')
TGTARCH=$(ssh -q root@$TGT 'uname -p')
TGTCENTRIFY_VER=$(ssh -q root@$TGT 'adinfo -v'|awk '{print $3}'|sed "s/)//")

if [ "${OS}" = "Linux" ]; then
		DCPKGNAME="centrifydc-5.1.1-rhel3-x86_64.rpm"
		CENTRIFY="${CENTRIFY}/RedHat"
else if [ "${OS}" = "SunOS" ]; then
		DCPKGNAME="CentrifyDC"
		CENTRIFY="${CENTRIFY}/Solaris-${TGTARCH}"
	fi
fi

if [ "${TGTCENTRIFY_VER}" != "${CENTRIFY_VER}" ]; then
		echo "${TGT} Centrify will to be upgraded from ${TGTCENTRIFY_VER} to ${CENTRIFY_VER}"
else 
	echo "${TGT} is up to date"
	exit 0
fi

scp -r ${CENTRIFY}/${DCPKGNAME} root@${TGT}:/root > /dev/null 2>&1
scp -r upgrade-standalone.sh root@${TGT}:/root > /dev/null 2>&1

ssh -q root@${TGT} '/root/upgrade-standalone.sh'