#!/bin/bash

OS=$(uname -s)
REL=$(uname -r)
TGTARCH=$(uname -p)

if [ "${OS}" = "Linux" ]; then
		DCPKGNAME="centrifydc-5.1.1-rhel3-x86_64.rpm"
else if [ "${OS}" = "SunOS" ]; then
		DCPKGNAME="CentrifyDC"
	fi
fi

if [ "${OS}" = "Linux" ]; then
	tar cf /root/centrify-backup.tar /etc/centrifydc/centrifydc.conf /etc/krb5.keytab /var/centrifydc/kset.* > /dev/null 2>&1 && rm -rf /etc/krb5/krb5.keytab /var/centrifydc/kset.* > /dev/null 2>&1
	yum localinstall -y --nogpgcheck /root/${DCPKGNAME} > /dev/null 2>&1
	cd / && tar xf /root/centrify-backup.tar > /dev/null 2>&1
	/etc/init.d/centrifydc restart > /dev/null 2>&1
fi

if [ "${OS}" = "SunOS" ]; then	
	tar cvf /root/centrify-backup.tar /etc/centrifydc/centrifydc.conf /etc/krb5/krb5.keytab /var/centrifydc/kset.* > /dev/null 2>&1 && rm -rf /etc/krb5/krb5.keytab /var/centrifydc/kset.* > /dev/null 2>&1 
	yes|pkgrm CentrifyDC > /dev/null 2>&1
	if [ "${REL}" = "5.11" ]; then
		svcadm restart svc:/system/manifest-import:default
	fi 
	yes|pkgadd -G -d /root/${DCPKGNAME} all > /dev/null 2>&1
	if [ "${REL}" = "5.11" ]; then
		svcadm restart svc:/system/manifest-import:default
	fi 
	cd / && tar xf /root/centrify-backup.tar > /dev/null 2>&1
	svcadm clear centrifydc > /dev/null 2>&1 
	svcadm enable centrifydc	> /dev/null 2>&1
fi
