#!/bin/bash
# Because some Linux servers have the root umask set to 027 and we need 022
umask 022

# This script is meant to run in order to fix some things post migration

#Checking we are indeed connected to AD
CSTATUS=$(adinfo -m)
OS=$(uname -s)

if [ "${OS}" != "Linux" ]; then
	echo "Script meant for Linux systems"
	exit 1
fi

if [ "${CSTATUS}" != "connected" ]; then
	echo "ERROR: Not connected to AD"
	exit 1
fi  

# krb5.conf logging configuration
if [ -f /etc/krb5.conf.pre_cdc ]; then
	cat /etc/krb5.conf.pre_cdc /etc/krb5.conf > /tmp/krb5.conf
	mv 	/tmp/krb5.conf /etc/krb5.conf
	/etc/init.d/centrify-kcm restart
fi

if [ -f /etc/profile.d/krb5-workstation.sh ]; then
	rm /etc/profile.d/krb5-workstation.*	
fi

# PAM configuration changes for allowing cron for locked accounts
grep -v "^adclient.autoedit.pam" /etc/centrifydc/centrifydc.conf > /tmp/centrifydc.conf.tmp
echo "adclient.autoedit.pam: false" >> /tmp/centrifydc.conf.tmp
mv -f /tmp/centrifydc.conf.tmp /etc/centrifydc/centrifydc.conf
adreload

# adding pam success if user is oracle, sybase or sybaseiq
cp /etc/pam.d/crond /etc/pam.d/crond.pre_cdc
echo "# PAM configuration for allowing oracle,sybase and sybaseiq to run crontabs when locked" > /tmp/crond.tmp
echo "#" >> /tmp/crond.tmp
echo "account    sufficient pam_succeed_if.so user in oracle:sybase:sybaseiq" >> /tmp/crond.tmp
grep -v "account.*pam_succeed.*oracle.*sybase.*" /etc/pam.d/crond >> /tmp/crond.tmp
mv -f /tmp/crond.tmp /etc/pam.d/crond


# Removing teledist from /etc/passwd
grep -v "^teledist" /etc/passwd > /root/passwd.bck && mv /root/passwd.bck /etc/passwd && pwconv
# Fixing ownership of teledist
chown -R teledist:6000 /export/home/teledist > /dev/null 2>&1
chown -R teledist:6000 /home/teledist > /dev/null 2>&1
#Running adfixid & adrmlocal

/usr/share/centrifydc/bin/adfixid -C -r /var/tmp/adfixid.out
/usr/share/centrifydc/bin/adrmlocal -f

# Fix Cert GP errors
chmod 666 /usr/share/centrifydc/mappers/machine/certgp.pl

#Stopping ctsa
/etc/init.d/agent-ctsa stop 
chkconfig agent-ctsa off
pkill p_ctsce

#Stopping sysload
pkill sdclient

#stopping sssd
service sssd stop
chkconfig sssd off

#Removing other accounts from password
egrep -v "^l[0-9][0-9]|^telesec|^ctsa|^app-rpm" /etc/passwd > /root/passwd.bck && mv /root/passwd.bck /etc/passwd && pwconv
egrep -v "^stdusers|^ctsa|^sysload" /etc/group > /root/group.bck && mv /root/group.bck /etc/group

#Removing sudo
#yum remove -y sudo
ln -s /usr/share/centrifydc/bin/sudo /usr/bin/sudo
	
#Restarting sysload
[ -x /etc/init.d/sysload-sdc ] && /etc/init.d/sysload-sdc start

#Restarting crond
service crond restart

#Restarting sshd
service centrify-sshd restart
service bnpa-ssh restart 

##############################################################################
#Section handling special oracle/sybase machine needs
#nsswitch.conf order change for groups. Since the oracle account has to keep its existing
#UID and GID the systems nees to map the GID correctly. We need to add files before centrifydc
adquery user oracle > /dev/null 2>&1
if [ $? -eq 0 ]; then	
	cat /etc/nsswitch.conf|sed "s/^group.*centrifydc.*/group: files centrifydc/g" > /tmp/nsswitch.tmp
	mv /tmp/nsswitch.tmp /etc/nsswitch.conf
	chmod 644 /etc/nsswitch.conf
	grep -v "^adclient.autoedit.nss" /etc/centrifydc/centrifydc.conf > /tmp/centrifydc.conf.tmp
	echo "adclient.autoedit.nss: false" >> /tmp/centrifydc.conf.tmp
	mv -f /tmp/centrifydc.conf.tmp /etc/centrifydc/centrifydc.conf
	
	#libmap.so needs to be installed on the system
	#The file is provided by the pam-devel package
	yum install -y pam-devel 
	
	#Creating pam link for emagent
	ln -s /etc/pam.d/system-auth /etc/pam.d/emagent
fi
RET=0
adquery user sybase > /dev/null 2>&1
if [ $? -eq 0 ]; then
	RET=1
fi
adquery user sybaseiq > /dev/null 2>&1
if [ $? -eq 0 ]; then
	RET=1
fi 

if [ ${RET} -eq 1 ]; then	
	cat /etc/nsswitch.conf|sed "s/^group.*centrifydc.*/group: files centrifydc/g" > /tmp/nsswitch.tmp
	mv /tmp/nsswitch.tmp /etc/nsswitch.conf
	chmod 644 /etc/nsswitch.conf
	grep -v "^adclient.autoedit.nss" /etc/centrifydc/centrifydc.conf > /tmp/centrifydc.conf.tmp
	echo "adclient.autoedit.nss: false" >> /tmp/centrifydc.conf.tmp
	mv -f /tmp/centrifydc.conf.tmp /etc/centrifydc/centrifydc.conf	
fi
#############################################################################
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "Output of adfixid in /var/tmp/adfixid.out"
echo "Backup passwd in /root/CENTRIFYBACKUP/etc/passwd"
echo "Backup group in /root/CENTRIFYBACKUP/etc/group"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
