#!/bin/bash
CENTRIFY="/bnp/files/Centrify"
CHECKMK="/bnp/files/check_mk"
CMKAGENT="check_mk-agent-solaris-current"
DCPKGNAME="CentrifyDC-latest"
SSHPKG="centrifydc-openssh-latest"
SCRIPTNAME="centrify-migrate-sol-standalone.sh"

if [ "x${USRNAME}" != "x" ]; then
	TGT="${USRNAME}@${1}"
else
	TGT="teledist@${1}"
fi

TGTARCH=`ssh -q ${TGT} 'uname -p'`

echo "Copying CentrifyDC"
scp -q ./fix-solaris.sh ${TGT}:/root
scp -q ${CENTRIFY}/Solaris-${TGTARCH}/${DCPKGNAME} ${TGT}:/root
scp -q ./${SCRIPTNAME} ${TGT}:/root
scp -q ./krb5.conf ${TGT}:/root
echo "Copying Centrify OpenSSH"
scp -q ${CENTRIFY}/Solaris-${TGTARCH}/${SSHPKG} ${TGT}:/root
echo "Copying Check MK Agent"
scp -q ${CHECKMK}/${CMKAGENT} ${TGT}:/root/check_mk_agent
scp -q -r ${CHECKMK}/local ${TGT}:/root/cmk-local
scp -q -r ${CHECKMK}/plugins ${TGT}:/root/cmk-plugins

ssh -q ${TGT} "/root/${SCRIPTNAME}"
RET=$?
if [ "${RET}" -ne 0 ]; then
	echo "ERROR deploying Centrify on ${TGT}"
	exit 1
fi
