#!/bin/bash


DCPKGNAME="CentrifyDC-latest"
SSHPKG="centrifydc-openssh-latest"


RELEASE=`uname -r`
TGTARCH=`uname -p`

# Enabling "upgrade" feature in pkgadd
if [ -f /var/sadm/install/admin/default ]; then
	cp /var/sadm/install/admin/default /root/default &&\
	cat /root/default|grep -v "instance=" > /root/default.tmp &&\
	echo "instance=overwrite" >>  /root/default.tmp &&\
	mv /root/default.tmp /root/default &&\
	cp /root/default /var/sadm/install/admin/default
fi

function zones_preinstall() {	
	if [ "x${ZONELIST}" = "x" ]; then
		return
	fi	
	
	for zone in ${ZONELIST}; do
		ZONEPATH=$(zonecfg -z ${zone} info zonepath|awk '{print $2}')
						
		# Backing up password & Co files in Zones
		rm -rf ${ZONEPATH}/root/root/CENTRIFYBACKUP.BAK
		[ -d ${ZONEPATH}/root/root/CENTRIFYBACKUP ] && mv ${ZONEPATH}/root/root/CENTRIFYBACKUP ${ZONEPATH}/root/root/CENTRIFYBACKUP.BAK 
		mkdir -p ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc 
		cp -f ${ZONEPATH}/root/etc/passwd ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc 
		cp -f ${ZONEPATH}/root/etc/shadow ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc
		cp -f ${ZONEPATH}/root/etc/group ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc
		cp -r ${ZONEPATH}/root/etc/ssh ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc
		[ -f ${ZONEPATH}/root/etc/krb5.conf ] && cp -f ${ZONEPATH}/root/etc/krb5.conf ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc
		[ -d ${ZONEPATH}/root/etc/krb5 ] && cp -rf ${ZONEPATH}/root/etc/krb5 ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc
		[ -d ${ZONEPATH}/root/etc/centrifydc ] && cp -rf ${ZONEPATH}/root/etc/centrifydc  ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc
		[ -f ${ZONEPATH}/root/etc/nsswitch.conf ] && cp -f ${ZONEPATH}/root/etc/nsswitch.conf ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc
		[ -d ${ZONEPATH}/root/etc/pam.d ] && cp -rf ${ZONEPATH}/root/etc/pam.d ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc
		[ -f ${ZONEPATH}/root/etc/pam.conf ] && cp -f ${ZONEPATH}/root/etc/pam.conf ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc
		[ -f ${ZONEPATH}/root/usr/lib/security/methods.cfg ] && cp -f ${ZONEPATH}/root/usr/lib/security/methods.cfg ${ZONEPATH}/root/root/CENTRIFYBACKUP
		[ -f ${ZONEPATH}/root/etc/security/user ] && cp -f ${ZONEPATH}/root/etc/security/user ${ZONEPATH}/root/root/CENTRIFYBACKUP
		
		# Copying fix-solaris.sh script to the zones
		cp /root/fix-solaris.sh ${ZONEPATH}/root/root/				
	done
}

function upgrade_zones() {	
	for zone in ${ZONELIST}; do
		ZONEPATH=$(zonecfg -z ${zone} info zonepath|awk '{print $2}')
		zlogin ${zone} 'pkginfo CentrifyDC > /dev/null 2>&1'
		if [ $? -eq 1 ]; then
			continue
		fi
		# Saving service status and disabling service for upgrade
		STATUS=$(zlogin ${zone} 'svcs centrifydc|egrep "enabled|disabled"'|awk '{print $1}')
		zlogin ${zone} 'svcadm disable svc:/application/security/centrifydc:default' > /dev/null 2>&1
		zlogin ${zone} 'svcadm disable svc:/application/security/centrify-kcm:default' > /dev/null 2>&1
		
		cat ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc/centrifydc/centrifydc.conf|egrep -v "^krb5.cache.type" > /tmp/${zone}-centrifydc.conf
		echo "krb5.cache.type: KCM" >> /tmp/${zone}-centrifydc.conf
		mv /tmp/${zone}-centrifydc.conf ${ZONEPATH}/root/etc/centrifydc/centrifydc.conf
		
		#Restarting centrify services if needed
		if [ "${STATUS}" = "enabled" ]; then
				zlogin ${zone} 'svcadm enable svc:/application/security/centrify-kcm:default' > /dev/null 2>&1
				zlogin ${zone} 'svcadm enable svc:/application/security/centrifydc:default' > /dev/null 2>&1	
		fi
	done	
}

function uninstall_native_ssh() {
	echo "Uninstalling system ssh"
	if [ ${RELEASE} = '5.11' ]; then
			pkg uninstall pkg://solaris/service/network/ssh_admin > /dev/null 2>&1 
			pkg uninstall pkg://solaris/service/network/ssh pkg://solaris/network/ssh/ssh-key  pkg://solaris/network/ssh pkg://solaris/archiver/gnu-tar > /dev/null 2>&1
			pkg uninstall sudo
	else 
			yes|pkgrm SUNWsshr > /dev/null 2>&1
			yes|pkgrm SUNWsshu > /dev/null 2>&1
			yes|pkgrm SUNWsshdr > /dev/null 2>&1
			yes|pkgrm SUNWsshdu > /dev/null 2>&1
			yes|pkgrm SUNWsshcu > /dev/null 2>&1		
	fi
	
	for zone in ${ZONELIST}; do
		ZONERELEASE=$(zlogin ${zone} 'uname -r')
		
		if [ ${ZONERELEASE} = '5.11' ]; then
			zlogin ${zone} 'pkg uninstall pkg://solaris/service/network/ssh_admin' > /dev/null 2>&1 
			zlogin ${zone} 'pkg uninstall pkg://solaris/service/network/ssh pkg://solaris/network/ssh/ssh-key  pkg://solaris/network/ssh pkg://solaris/archiver/gnu-tar' > /dev/null 2>&1			
		else 
			zlogin ${zone} 'yes|pkgrm SUNWsshr' > /dev/null 2>&1
			zlogin ${zone} 'yes|pkgrm SUNWsshu' > /dev/null 2>&1
			zlogin ${zone} 'yes|pkgrm SUNWsshdr' > /dev/null 2>&1
			zlogin ${zone} 'yes|pkgrm SUNWsshdu' > /dev/null 2>&1
			zlogin ${zone} 'yes|pkgrm SUNWsshcu' > /dev/null 2>&1		
		fi	
	done	
}

function install_openssh_zones() {
	for zone in ${ZONELIST}; do
		ZONEPATH=$(zonecfg -z ${zone} info zonepath|awk '{print $2}')
		ZONERELEASE=$(zlogin ${zone} 'uname -r')
		
		if [ ! -d ${ZONEPATH}/root/usr/lib/ssh ]; then
			mkdir ${ZONEPATH}/root/usr/lib/ssh
		fi
		
		cp -r ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc/ssh/* ${ZONEPATH}/root/etc/centrifydc/ssh
		
		if [ ${ZONERELEASE} = '5.11' ]; then
			cp -r ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc/ssh/* ${ZONEPATH}/root/etc/centrifydc/ssh/
			egrep -v "^HostKey|^Subsystem.*sftp.*|^ChallengeResponseAuthentication|^MaxAuthTriesLog|^PAMAuthenticationViaKBDInt|^RhostsAuthentication" ${ZONEPATH}/root/etc/centrifydc/ssh/sshd_config > ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
			echo "ChallengeResponseAuthentication yes" >> ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
			echo "HostKey /etc/centrifydc/ssh/ssh_host_rsa_key" >> ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
			echo "HostKey /etc/centrifydc/ssh/ssh_host_dsa_key" >> ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
			echo "Subsystem       sftp    /usr/share/centrifydc/libexec/sftp-server" >> ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
			echo "DefEnvPATH /usr/share/centrifydc/sbin:/usr/share/centrifydc/kerberos/bin:/usr/share/centrifydc/kerberos/sbin:/usr/share/centrifydc/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin" >> ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\ 
			mv ${ZONEPATH}/root/tmp/sshd_conf.tmp ${ZONEPATH}/root/etc/centrifydc/ssh/sshd_config &&\
			zlogin ${zone} '/etc/init.d/centrify-sshd stop; /etc/init.d/centrify-sshd start' &&\
			zlogin ${zone} 'ln -s /usr/share/centrifydc/sbin/sshd /usr/lib/ssh/sshd'
			zlogin ${zone} 'svcadm clear bnpssh' >/dev/null 2>&1
			zlogin ${zone} 'svcadm restart bnpssh'
		else
				# Post-instsall bnpa-ssh sshd_config file
				cat ${ZONEPATH}/root/usr/local/etc_admin/sshd_config|egrep -v "^UsePAM|^HostKey|^Subsystem.*sftp.*" > ${ZONEPATH}/root/tmp/bnpa-sshd_config &&\
				echo "HostKey /etc/centrifydc/ssh/ssh_host_rsa_key" >> ${ZONEPATH}/root/tmp/bnpa-sshd_config  &&\
				echo "HostKey /etc/centrifydc/ssh/ssh_host_dsa_key" >> ${ZONEPATH}/root/tmp/bnpa-sshd_config &&\
				echo "UsePAM yes" >> ${ZONEPATH}/root/tmp/bnpa-sshd_config &&\
				echo "Subsystem       sftp    /usr/share/centrifydc/libexec/sftp-server" >>  ${ZONEPATH}/root/tmp/bnpa-sshd_config &&\
				mv -f ${ZONEPATH}/root/tmp/bnpa-sshd_config ${ZONEPATH}/root/usr/local/etc_admin/sshd_config
		
				cp ${ZONEPATH}/root/root/CENTRIFYBACKUP/etc/ssh/sshd_config ${ZONEPATH}/root/etc/centrifydc/ssh/sshd_config &&\
				egrep -v "^UsePAM|^HostKey|^Subsystem.*sftp.*|^ChallengeResponseAuthentication|^MaxAuthTriesLog|^PAMAuthenticationViaKBDInt|^RhostsAuthentication" ${ZONEPATH}/root/etc/centrifydc/ssh/sshd_config > ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
				echo "ChallengeResponseAuthentication yes" >> ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
				echo "HostKey /etc/centrifydc/ssh/ssh_host_rsa_key" >> ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
				echo "HostKey /etc/centrifydc/ssh/ssh_host_dsa_key" >> ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
				echo "Subsystem       sftp    /usr/share/centrifydc/libexec/sftp-server" >> ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
				echo "UsePAM yes" >> ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
				echo "DefEnvPATH /usr/share/centrifydc/sbin:/usr/share/centrifydc/kerberos/bin:/usr/share/centrifydc/kerberos/sbin:/usr/share/centrifydc/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin" >> ${ZONEPATH}/root/tmp/sshd_conf.tmp &&\
				mv ${ZONEPATH}/root/tmp/sshd_conf.tmp ${ZONEPATH}/root/etc/centrifydc/ssh/sshd_config &&\
				zlogin ${zone} '/etc/init.d/centrify-sshd stop && /etc/init.d/centrify-sshd start'
				zlogin ${zone} 'ln -s /usr/share/centrifydc/sbin/sshd /usr/lib/ssh/sshd'
				zlogin ${zone} 'svcadm clear ssh_admin' > /dev/null 2>&1
				zlogin ${zone} 'svcadm restart ssh_admin && rm -rf /etc/ssh'			
		fi
	done
}

function upgrade_openssh_zones() {
	for zone in ${ZONELIST}; do
			ZONEPATH=$(zonecfg -z ${zone} info zonepath|awk '{print $2}')
			ZONERELEASE=$(zlogin ${zone} 'uname -r')
			
			# Backing up the configuration file because the new package will overwrite it
    		cp ${ZONEPATH}/root/etc/centrifydc/ssh/sshd_config ${ZONEPATH}/root/root/sshd_config
    		
    		
    		# copy sshd_config file
    		mv ${ZONEPATH}/root/root/sshd_config ${ZONEPATH}/root/etc/centrifydc/ssh/sshd_config
    		
    		# Restarting Centrify services	
			SSHD_PID=$(zlogin ${zone} 'ps -aef'|grep 'centrifydc/sbin/sshd'|awk '{if($3 == 1){print $2}}')
			if [ "x${SSHD_PID}" != "x" ]; then
				kill -HUP ${SSHD_PID}
			else		
				zlogin ${zone} '/etc/init.d/centrify-sshd stop; /etc/init.d/centrify-sshd start'
			fi
			
			# Restarting ssh_admin
			if [ ${ZONERELEASE} = '5.11' ]; then
					SSHD_PID=$(zlogin ${zone} 'ps -aef'|grep "/etc/bnpssh/sshd_config$"|awk '{if($3 == 1){print $2}}')
					if [ "x${SSHD_PID}" != "x" ]; then
						kill -HUP ${SSHD_PID}
					else
						zlogin ${zone} 'svcadm clear bnpssh' > /dev/null 2>&1
						zlogin ${zone} 'svcadm restart bnpssh'
					fi					
			else			
				SSHD_PID=$(zlogin ${zone} 'ps -aef'|grep "/usr/local/etc_admin/sshd_config$"|awk '{if($3 == 1){print $2}}')
				if [ "x${SSHD_PID}" != "x" ]; then						 
						zlogin ${zone} 'svcadm clear svc:/network/ssh_admin:default'	2>&1
						kill -HUP ${SSHD_PID}						
				else	
					zlogin ${zone} 'svcadm clear svc:/network/ssh_admin:default'	2>&1	
					zlogin ${zone} 'svcadm restart svc:/network/ssh_admin:default'
				fi
			fi			
	done
}

# Getting list of zones installed on the server
ZONELIST=$(zoneadm list |grep -v "global")

# Making a list of all the sparse zones. Removing whole zones from list
#_TMPLIST=""
#for zone in ${ZONELIST}; do
#	SPARSE=$(zonecfg -z ${zone} info inherit-pkg-dir)
#	if [ "x${SPARSE}" = "x" ]; then
#		_TMPLIST="${_TMPLIST} ${zone}"
	#	fi
#done
#ZONELIST=${_TMPLIST}

# Handling whole zone case. Whole zones should be handled like physical machines
ZONENAME=$(zonename)
if [ "${ZONENAME}" != global ]; then
	ZONELIST=""
fi

# Create a config backup for each zone
zones_preinstall

# Installation of CentrifyDC in the global zone and the other zones
_PKGNAME=`pkginfo CentrifyDC|awk '{print $2}'`
if [ "${_PKGNAME}" = "CentrifyDC" ]; then
        echo "Package already installed. Checking for upgrade"
        _PKGVER=`pkginfo -l CentrifyDC|grep VERSION|awk -F: '{print $2}'`
        DCPKGVER=`pkginfo -l -d /root/${DCPKGNAME}|grep VERSION|awk -F: '{print $2}'`
        if [ "${_PKGVER}" = "${DCPKGVER}" ]; then
        	echo "Package CentrifyDC already up-to-date."
    	else    		
    		# Services need to be stopped before upgrading
    		svcadm disable svc:/application/security/centrifydc:default > /dev/null 2>&1
    		svcadm disable svc:/application/security/centrify-kcm:default > /dev/null 2>&1
    		# Backing up the configuration file because the new package will overwrite it
    		cp /etc/centrifydc/centrifydc.conf /root/centrifydc.conf
    		
			yes|pkgadd -d /root/${DCPKGNAME} all > /dev/null 2>&1
    		if [ $? -eq 1 ]; then    			
    				yes|pkgadd -G -d /root/${DCPKGNAME} all > /dev/null 2>&1	
    		fi
    		
    		# Activating in-memory kerberos cache in backup configuration file and deploying it
			# in the centrify config directory
    		cat /root/centrifydc.conf|egrep -v "^krb5.cache.type"  > /root/centrifydc.conf.tmp &&\
    		echo "krb5.cache.type: KCM" >> /root/centrifydc.conf.tmp &&\
    		mv /root/centrifydc.conf.tmp /root/centrifydc.conf
    		cp /root/centrifydc.conf /etc/centrifydc/centrifydc.conf
    		
    		# Restarting Centrify services
    		svcadm enable svc:/application/security/centrify-kcm:default > /dev/null 2>&1
    		svcadm enable svc:/application/security/centrifydc:default 
    		adreload   	
    		
    		upgrade_zones;		    		
    	fi
else		
	yes|pkgadd -d /root/${DCPKGNAME} all > /dev/null 2>&1
	echo "Checking Package installation"
	_PKGNAME=`pkginfo CentrifyDC|awk '{print $2}'`
	if [ "${_PKGNAME}" != "CentrifyDC" ]; then
		echo "ERROR installing CentrifyDC"
		exit 1
	fi
	
	#Activating KCM in-memory Kerberos Ticket for global and zones
	echo "krb5.cache.type: KCM" >> /etc/centrifydc/centrifydc.conf
	for zone in ${ZONELIST}; do
			ZONEPATH=$(zonecfg -z ${zone} info zonepath|awk '{print $2}')
			echo "krb5.cache.type: KCM" >> ${ZONEPATH}/root/etc/centrifydc/centrifydc.conf
	done
fi


[ -f /etc/ssh/sshd_config ] && cp /etc/ssh/sshd_config /root/sshd_config
[ -f /etc/centrifydc/ssh/sshd_config ] && cp /etc/centrifydc/ssh/sshd_config /root/sshd_config

# Uninstalling native SSH server 
uninstall_native_ssh


echo "Installing Centrify OpenSSH"
_PKGNAME=`pkginfo CentrifyDC-openssh|awk '{print $2}'`
if [ "${_PKGNAME}" = "CentrifyDC-openssh" ]; then
	echo "Package Centrify OpenSSH already installed. Checking for upgrade"
	_SSHVER=`pkginfo -l CentrifyDC-openssh|grep VERSION|awk -F: '{print $2}'`
	SSHPKGVER=`pkginfo -l -d /root/${SSHPKG}|grep VERSION|awk -F: '{print $2}'`
	if [ "${_SSHVER}" = "${SSHPKGVER}" ]; then
        	echo "Package Centrify OpenSSH already up-to-date."
    else
    		# Backing up the configuration file because the new package will overwrite it
    		cp /etc/centrifydc/ssh/sshd_config /root/sshd_config
    		
    		yes|pkgadd -d /root/${SSHPKG} all > /dev/null 2>&1
    		if [ $? -eq 1 ]; then    			
    				yes|pkgadd -G -d /root/${SSHPKG} all > /dev/null 2>&1	
    		fi
    		# copy sshd_config file
    		mv /root/sshd_config /etc/centrifydc/ssh/sshd_config
    		
    		# Restarting Centrify services	
			SSHD_PID=$(ps -aef|grep 'centrifydc/sbin/sshd'|awk '{if($3 == 1){print $2}}')
			if [ "x${SSHD_PID}" != "x" ]; then
				kill -HUP ${SSHD_PID}
			else		
				/etc/init.d/centrify-sshd stop; /etc/init.d/centrify-sshd start
			fi
			
			# Restarting ssh_admin
			if [ ${RELEASE} = '5.11' ]; then
					SSHD_PID=$(ps -aef|grep "/etc/bnpssh/sshd_config$"|awk '{if($3 == 1){print $2}}')
					if [ "x${SSHD_PID}" != "x" ]; then
						kill -HUP ${SSHD_PID}
					else
						svcadm clear bnpssh > /dev/null 2>&1
						svcadm restart bnpssh
					fi					
			else			
				SSHD_PID=$(ps -aef|grep "/usr/local/etc_admin/sshd_config$"|awk '{if($3 == 1){print $2}}')
				if [ "x${SSHD_PID}" != "x" ]; then						 
						svcadm clear svc:/network/ssh_admin:default	2>&1
						kill -HUP ${SSHD_PID}						
				else	
					svcadm clear svc:/network/ssh_admin:default	2>&1	
					svcadm restart svc:/network/ssh_admin:default
				fi
			fi		
			upgrade_openssh_zones				
    fi
else
	yes|pkgadd -d /root/${SSHPKG} all > /dev/null 2>&1
	echo "Checking Package installation"
	_PKGNAME=`pkginfo CentrifyDC-openssh|awk '{print $2}'`
	if [ "${_PKGNAME}" != "CentrifyDC-openssh" ]; then
		echo "ERROR installing Centrify OpenSSH"
		exit 1
	fi	
	if [ ! -d /usr/lib/ssh ]; then
		mkdir /usr/lib/ssh
	fi
		
	if [ ! -d /etc/ssh ]; then
		mkdir /etc/ssh		
	fi
	cp -r /root/CENTRIFYBACKUP/etc/ssh/* /etc/centrifydc/ssh
	if [ ${RELEASE} = '5.11' ]; then		
		cp /root/sshd_config /etc/centrifydc/ssh/sshd_config &&\
		egrep -v "^UsePAM|^HostKey|^Subsystem.*sftp.*|^ChallengeResponseAuthentication|^MaxAuthTriesLog|^PAMAuthenticationViaKBDInt|^RhostsAuthentication" /etc/centrifydc/ssh/sshd_config > /tmp/sshd_conf.tmp &&\
		echo "ChallengeResponseAuthentication yes" >> /tmp/sshd_conf.tmp &&\
		echo "HostKey /etc/centrifydc/ssh/ssh_host_rsa_key" >> /tmp/sshd_conf.tmp &&\
		echo "HostKey /etc/centrifydc/ssh/ssh_host_dsa_key" >> /tmp/sshd_conf.tmp &&\
		echo "Subsystem       sftp    /usr/share/centrifydc/libexec/sftp-server" >> /tmp/sshd_conf.tmp &&\
		echo "UsePAM yes" >> /tmp/sshd_conf.tmp &&\
		echo "DefEnvPATH /usr/share/centrifydc/sbin:/usr/share/centrifydc/kerberos/bin:/usr/share/centrifydc/kerberos/sbin:/usr/share/centrifydc/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin" >> /tmp/sshd_conf.tmp &&\
		mv /tmp/sshd_conf.tmp /etc/centrifydc/ssh/sshd_config &&\		
		/etc/init.d/centrify-sshd stop && /etc/init.d/centrify-sshd start &&\
		ln -s /usr/share/centrifydc/sbin/sshd /usr/lib/ssh/sshd
		svcadm clear bnpssh > /dev/null 2>&1 
		svcadm restart bnpssh
		if [ ! -L /etc/ssh ]; then
			rm -rf /etc/ssh 
		fi
	else
		# Post-instsall bnpa-ssh sshd_config file
		cat /usr/local/etc_admin/sshd_config|egrep -v "^UsePAM|^HostKey|^Subsystem.*sftp.*" > /tmp/bnpa-sshd_config &&\
		echo "HostKey /etc/centrifydc/ssh/ssh_host_rsa_key" >> /tmp/bnpa-sshd_config  &&\
		echo "HostKey /etc/centrifydc/ssh/ssh_host_dsa_key" >> /tmp/bnpa-sshd_config &&\
		echo "UsePAM yes" >> /tmp/bnpa-sshd_config &&\
		echo "Subsystem       sftp    /usr/share/centrifydc/libexec/sftp-server" >>  /tmp/bnpa-sshd_config &&\
		mv -f /tmp/bnpa-sshd_config /usr/local/etc_admin/sshd_config
		
		
		cp /root/sshd_config /etc/centrifydc/ssh/sshd_config &&\
		cat /etc/centrifydc/ssh/sshd_config|egrep -v "^UsePAM|^HostKey|^Subsystem.*sftp.*|^ChallengeResponseAuthentication|^MaxAuthTriesLog|^PAMAuthenticationViaKBDInt|^RhostsAuthentication"  > /tmp/sshd_conf.tmp &&\
		echo "ChallengeResponseAuthentication yes" >> /tmp/sshd_conf.tmp &&\
		echo "HostKey /etc/centrifydc/ssh/ssh_host_rsa_key" >> /tmp/sshd_conf.tmp &&\
		echo "HostKey /etc/centrifydc/ssh/ssh_host_dsa_key" >> /tmp/sshd_conf.tmp &&\
		echo "Subsystem       sftp    /usr/share/centrifydc/libexec/sftp-server" >> /tmp/sshd_conf.tmp &&\
		echo "UsePAM yes" >> /tmp/sshd_conf.tmp &&\
		echo "DefEnvPATH /usr/share/centrifydc/sbin:/usr/share/centrifydc/kerberos/bin:/usr/share/centrifydc/kerberos/sbin:/usr/share/centrifydc/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin" >> /tmp/sshd_conf.tmp &&\
		mv /tmp/sshd_conf.tmp /etc/centrifydc/ssh/sshd_config &&\
		/etc/init.d/centrify-sshd stop && /etc/init.d/centrify-sshd start &&\
		ln -s /usr/share/centrifydc/sbin/sshd /usr/lib/ssh/sshd &&\
		svcadm clear ssh_admin > /dev/null 2>&1
		svcadm restart ssh_admin && rm -rf /etc/ssh 
	fi		
	install_openssh_zones
fi


#Check MK installation
mkdir -p /opt/BNPmonitor/{bin,local,plugins}
cp /root/check_mk_agent /opt/BNPmonitor/bin
chmod 755 /opt/BNPmonitor/bin/check_mk_agent
cp -r /root/cmk-local/* /opt/BNPmonitor/local
cp -r /root/cmk-plugins /opt/BNPmonitor/plugins
chmod -R 755  /opt/BNPmonitor/local
chmod -R 755  /opt/BNPmonitor/plugins
echo "check_mk        6556/tcp" >> /etc/services
echo "check_mk stream tcp nowait root //usr/sfw/sbin/tcpd /opt/BNPmonitor/bin/check_mk_agent" >> /etc/inet/inetd.conf
/usr/sbin/inetconv > /dev/null 2>&1

for zone in ${ZONELIST}; do
		ZONEPATH=$(zonecfg -z ${zone} info zonepath|awk '{print $2}')
						
		# Backing up password & Co files in Zones
		mkdir -p ${ZONEPATH}/root/opt/BNPmonitor/{bin,local,plugins}
		cp /root/check_mk_agent ${ZONEPATH}/root/opt/BNPmonitor/bin > /dev/null 2>&1
		chmod 755 ${ZONEPATH}/root/opt/BNPmonitor/bin/check_mk_agent > /dev/null 2>&1
		cp -r /root/cmk-local/* ${ZONEPATH}/root/opt/BNPmonitor/local/ > /dev/null 2>&1
		cp -r /root/cmk-plugins/*		${ZONEPATH}/root/opt/BNPmonitor/plugins/ > /dev/null 2>&1
		chmod -R 755  ${ZONEPATH}/root/opt/BNPmonitor/plugins > /dev/null 2>&1
		chmod -R 755  ${ZONEPATH}/root/opt/BNPmonitor/local > /dev/null 2>&1
		echo "check_mk        6556/tcp" >> ${ZONEPATH}/root/etc/services
		echo "check_mk stream tcp nowait root /usr/sfw/sbin/tcpd /opt/BNPmonitor/bin/check_mk_agent" >> ${ZONEPATH}/root/etc/inet/inetd.conf
	zlogin ${zone} "/usr/sbin/inetconv" 
done

#Kerberos confirguation to remove KCM logging
#cp -f /root/krb5.conf /etc/krb5/krb5.conf