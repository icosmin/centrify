#!/bin/ksh

LOG=/var/tmp/rhupgrade.log
tee -a ${LOG} >&2 |&
exec >&p 2>&1

debug=""
force=""
file=""
nodes=""
options=""
app="default"
user="root"
color=1
all=0
solaris=0
dry_run="echo "

AWK=awk
[ -x /bin/nawk ] && AWK=nawk

basedir=/var/tmp/sssd2ad

#

usage(){
  cat <<EOF
  $(basename $0) <options>
	options:
	-f          		 do it!
	-n|--dry_run 		 dry run : don't do anything (default)
	-d|--debug 			 activate debugging mode
	-a|--app 			   server grouped by app affinity...
	-h|--help 			 this help
	-w|--nodes 			 server list separate with comma
	-l|--file 			 server list in file, one per line
	-u|--user 			 user. Default: root
EOF
        exit 1
}
#

myoptions=$(getopt -o a:cdfhi:l:nsu:w: -l help,color,debug,force,dry_run,app:,file:,nodes:,user: -- "$@")
[ $? -ne 0 ]&&usage

eval set -- "$myoptions"          
while true
do
  case "$1" in
  -a|--app) app=$2; shift 2;;
  -c|--color) color=0; shift 1;;
  -f|--force) dry_run=""; shift 1;;
  -d|--debug) debug="echo "; shift 1;;
  -i|--nodes) options="-o \"-oIdentityFile=$2\""; shift 2;;
  -l|--file) file=$2; shift 2;;
  -n|--dry_run) dry_run="echo "; shift 1;;
  -s) solaris=1; shift 1;;
  -w|--nodes) nodes=$2; shift 2;;
  -u|--user) user=$2; shift 2;;
  --) shift 1; break ;;
  -h|--help) usage
  esac
done

if [ ${color} -eq 1 ]; then
_esc=""

set -a
_FG_RED="$_esc[31m"
_FG_GREEN="$_esc[32m"
_FG_YELLOW="$_esc[33m"
_FG_ORANGE="$_esc[0;33m"
_BG_RED="$_esc[41m"
_BG_GREEN="$_esc[42m"
_BG_YELLOW="$_esc[43m"
_BG_ORANGE="$_esc[0;43m"
_BOLD="$_esc[1m"
_RESET="$_esc[0m"
_UNDERLINE="$_esc[4m"
fi
#
#
#
[ "X${debug}" = "X" ] || set -x
if [ "X${nodes}" = "X" ] ; then
  if [ "X${file}" = "X" ] ; then
    echo "WARN: you need to provide either -w or -l switch"
    usage
  else
    if [ -f ${file} ]  ; then
      nodes=$(${AWK} '!/#/' ${file} | paste -d ',' -s)
    else
      echo "ERROR: file ${file} don't exist! "
    fi
  fi
fi

[ -d ${basedir} ] || mkdir ${basedir}
#
#
mygetent(){
if [ ${solaris} -eq 1 -o "X${user}" = "Xteledist" ] ; then
  AWK=nawk
  user="teledist"
fi

clush -l ${user} -bqw ${tnodes} '
'"'${AWK}'"' '\''
BEGIN{
  basedir="'"${basedir}"'"
  #
  c="[ -d "basedir" ]||mkdir -p "basedir;while(c|getline ok>0){};close(c)
  userfile=basedir"/sssd2passwd"
  groupfile=basedir"/sssd2group"
  #
  issssd=1
  c="[ -x /etc/init.d/sssd ]&&/etc/init.d/sssd status 2>/dev/null|grep -c running"
  c|getline issssd;close(c)
  #
  c="/etc/passwd"
  while(getline ok < c >0){
    split(ok,r,":")  
    usertab[r[1]]=ok
  }
  close(c)
  #
  c="/etc/group"
  while(getline ok < c >0){
    split(ok,r,":")  
    grouptab[r[1]]=ok
  }
  close(c)
  #
  while(getline ok < "/etc/security/access.conf" > 0){
    split(ok,tab," ")
    user=tab[2]
    if (ok~/^+/&&user!="ALL"){
      sub(":","",user)
      if (user~/^@/){
        sub("@","",user)
        getnetgroup(user)
      }else{
        c="getent passwd "user" 2>/dev/null"
        c|getline ok;close(c)
        usertab[user]=ok
      }
    }
  }
  close("/etc/security/access.conf")
#
  for (user in usertab) {
    print usertab[user] > userfile
    c="getent passwd "user
    while(c|getline ok>0){
     split(ok,r,":")
#     print user,r[4]
      group=gidtab[r[4]]
      if (length(group)>0){
        c1="getent group "group" 2>/dev/null"
        c1|getline ok1;close(c1)
        grouptab[group]=ok1
      }
    }
    close(c)    
  }
  #
  getgroup()
  for (group in grouptab) {
    print grouptab[group] > groupfile
  }
  #
}
  #
function getnetgroup(name, user,c,c1,n,i,ok,ok1,r){
  if (issssd==0) return
  c="getent netgroup "name
  while(c|getline ok>0){
    n=split(ok,r,"(,)")
    for (i=2;i<=n;i++){
      user=r[i]
      gsub(/,| |:/,"",user)
      sub(/\(|\)/,"",user)
      if (user!~/\(|\)/){
        c1="getent passwd "user" 2>/dev/null"
        c1|getline ok1;close(c1)
        usertab[user]=ok1
      } 
    } 
  }    
  close(c)    
}
#
function getgroup(){
  c="getent group 2>/dev/null"
  while(c|getline ok>0){
    split(ok,r,":")
    gidtab[r[3]]=r[1]
    n=split(r[4],t,",")
    for (i=1;i<=n;i++){
      if (t[i] in usertab){
#        print t[i]
        if (length(r[1])>0) grouptab[r[1]]=ok
      }
    }
  }
  close(c)
}
#
'\''' 


clush -w ${tnodes} -o "-oUser=${user}" --rcopy ${basedir}/sssd2group ${basedir}/sssd2passwd --dest ${basedir} 2>/dev/null
}
#
#
#
#set -x
tnodes=""
for host in $(echo ${nodes}|${AWK} -F"," '{for (i=1;i<=NF;i++) print $i}');do
  if [ ! -f ${basedir}/sssd2passwd.${host} -o ! -f ${basedir}/sssd2group.${host} ] ; then
    [ "X${tnodes}" = "X" ] && tnodes=${host} || tnodes=${tnodes}",${host}" 
  fi
done

[ "X${tnodes}" = "X" ] || mygetent ${tnodes}
#exit
if [ "X${nodes}" != "X" ] ; then

[ -x /bin/nawk ] && AWK=nawk || AWK=awk

${AWK} -v nodes=${nodes} -v basedir=${basedir} '
BEGIN{
  red="'"${_BOLD}${_BG_RED}"'"
  green="'"${_BOLD}${_BG_GREEN}"'"
  orange="'"${_BOLD}${_FG_ORANGE}"'"
  reset="'"${_RESET}"'"
#
  count=1
#
  app="'"${app}"'"
#
  getallgroups()
  getallusers()
#
}
#
function getgroups(host, file,isfile,ok,c,r,name,gid){
    file=basedir"/sssd2group."host
    c="[ -f "file" ]&&echo 1||echo 0"
    c|getline isfile
    close(c)
    if (isfile){
# file exist
      while(getline ok < file > 0){
        n=split(ok,r,":")
        name=r[1]
        gid=r[3]
#
        if (name~/^(root)$/) continue
#
	        if (gid in hosts){
# group have been already found
# we only work on user with different gid
#	          if (name=="postgres") printf "host -> %s %s\n",host,gid
	          split(groups[gid],t,":")
	          if (name!=t[1]){
	            hosts[gid]=hosts[gid]","host
# If duplicate id...
	            duplicate[gid]=groups[gid]
	          }
	        }else{
#	          if (name=="postgres") printf "host duplicate -> %s %s\n",host,gid
	          hosts[gid]=host
# check if group name already exist with another gid!
	          if (isused(name,gid,groups,ok)==0){
	            groups[gid]=ok
            }
	        }
      }
      close(file)
    }else{
      printf "ERROR: file %s don\047t exist!  \n",file
    }
}
#
function getusers(host){
    file=basedir"/sssd2passwd."host
    c="[ -f "file" ]&&echo 1||echo 0"
    c|getline isfile
    close(c)
    if (isfile){
# file exist
      while(getline ok < file > 0){
        n=split(ok,r,":")
        name=r[1]
        uid=r[3]
#
        if (name~/^dhcpd|^saslauth|^abrt|^nfsnobody|^ctsa|^root|^mail|^games|^sync|^ntp|^bin|^adm|^shutdown|^apache|^app-rpm|^rpc|^operator|^ftp|^gopher|^uucp|^avahi|^haldaemon|^halt|^nobody|^tcpdump|^postfix|^vcsa|^dbus|^lp|^daemon|^sshd|^telesec|^named|^news|^noaccess|^nrpe|^nscd|^oracle|^pcap|^postgres|^rpm|^smmsp|^squid|^ssadmin|^ssconfig|^ssmon|^svctag|^sys:|^teledist|^toto|^webservd|^dimstat|^distcache|^gdma|^gdm|^jabber|^lighttpd|^listen|^puppet|^ricci|^sys:|^bin:|^nobody|^tcpdump|^allstart|^aiuser|^bnppara|^dhcpserv|^dladm|^geneqdtools|^installed|^mysql|^n1gsps|^netadm|^netcfg|^nuucp|^openldap|^pkg5srv|^sc\-|^scncon|^scndb|^scn:|^tomcat|^tunn_usr|^ucs\-sds|^uce\-sds|^unknown|^upnp|^xvm:|^zfssnap|^n1sps|^2cadmin|^hacluster/) continue
#
	        if (uid in hosts){
#            if (name=="l244277") print "second ",count++,ok
# user have been already found
# we only work on user with different uid
	          split(users[uid],t,":")
	          if (name!=t[1]){
#	          if (name=="l244277") print "duplicate ",count++,ok
#	            if (uid==6003) printf "host %s uid %s name %s (%s)\n",host,uid,name,t[1]
	            hosts[uid]=hosts[uid]","host
#	            sub(t[1]":"t[2]":"t[3],"",users[uid])
#	            users[uid]=sprintf("%s:%s:%s%s%s:%s",t[1],t[2],red,t[3],reset,users[uid])
# If duplicate id...
	            duplicate[uid]=users[uid]
#	            duplicate[uid]=sprintf("%s:%s:%s%s%s:",r[1],r[2],red,r[3],reset)
#	            for (j=4;j<=n;j++) duplicate[uid]=sprintf("%s:%s",duplicate[uid],r[j])
#	            duplicate[uid]=sprintf("%s: (%s%s%s)",duplicate[uid],red,host,reset)
	          }
	        }else{
	          hosts[uid]=host
# check if user name already exist with another uid!
	          if (isused(name,uid,users,ok)){
#	            sub(name,"",users[uid])
#	            users[uid]=sprintf("%s%s%s%s",red,name,reset,users[uid])
	          } else {
#	          if (name=="l244277") print "first ",count++,ok
	            users[uid]=ok
            }
	        }
      }
      close(file)
    }else{
      printf "ERROR: file %s don\047t exist!  \n",file
    }
}
#
function getallgroups(){ 
#
  nbl=split(nodes,raw,",")
  for (i=1;i<=nbl;i++){
    host=raw[i]
    getgroups(host)
  }
#
  destdir=basedir"/"app
  c=sprintf("[ -d %s ]||mkdir -p %s",destdir,destdir)
  while(c|getline ok>0) print ok;close(c)
# 
 if (app=="default"){
    tnodes=nodes
    gsub(",","_",tnodes) 
    groupfile=destdir"/"tnodes".group"
    duplicatefile=destdir"/"tnodes".duplicate.group"
    conflictfile=destdir"/"tnodes".conflict.group"
  }else{
    groupfile=destdir"/"app".group"
    duplicatefile=destdir"/duplicate.group"
    conflictfile=destdir"/conflict.group"
  }
# 
  for (gid in groups){
	  split(groups[gid],t,":")
	  groupname=t[1]
#
	  if (gid==0){
	            printf "%s:%s:%s%s%s:",t[1],t[2],red,t[3],reset > groupfile
	            for (j=4;j<n;j++) printf "%s:",t[j] > groupfile
	            printf "%s\n",t[n]  >> groupfile

	  }else{
      if (length(groups[gid])>0) printf "%s\n",groups[gid] > groupfile
    }
  }
#
  for (i in conflict){
      split(i,r,"_")
      host=r[3]
#      file=conflictfile"."host
      file=conflictfile
      printf "%s\n",conflict[i] > file
  }
#
  for (i in hosts) delete hosts[i]
}
#
function getallusers(){ 
#
  nbl=split(nodes,raw,",")
  for (i=1;i<=nbl;i++){
    host=raw[i]
    getusers(host)
  }
#
  destdir=basedir"/"app
  c=sprintf("[ -d %s ]||mkdir -p %s",destdir,destdir)
  while(c|getline ok>0) print ok;close(c)
# 
 if (app=="default"){
    tnodes=nodes
    gsub(",","_",tnodes) 
    userfile=destdir"/"tnodes".passwd"
    duplicatefile=destdir"/"tnodes".duplicate.passwd"
    conflictfile=destdir"/"tnodes".conflict.passwd"
  }else{
    userfile=destdir"/"app".passwd"
    duplicatefile=destdir"/duplicate.passwd"
    conflictfile=destdir"/conflict.passwd"
  }
# 
  for (uid in users){
	  split(users[uid],t,":")
	  username=t[1]
#
	  if (uid==0){
#	            printf "%s:%s:%s%s%s:",t[1],t[2],red,t[3],reset > userfile
#	            for (j=4;j<n;j++) printf "%s:",t[j] > userfile
#	            printf "%s\n",t[n]  >> userfile
              printf "%s\n",users[uid] > userfile

	  }else{
#      if (uid in duplicate) {
#        printf "%s\n",duplicate[uid]  > duplicatefile
#      } 
#      if (isconflict(username)) continue
#      else printf "%s\n",users[uid] > userfile
      if (length(users[uid])>0) printf "%s\n",users[uid] > userfile
    }
  }
#
  for (i in conflict){
      split(i,r,"_")
      host=r[3]
#      file=conflictfile"."host
      file=conflictfile
      printf "%s\n",conflict[i] > file
  }
#
  for (i in hosts) delete hosts[i]
}
#
function isconflict(name, i,r){
  for (i in conflict){
    split(i,r,"_")
    if (name==r[1]) return 1
  }
#
  return 0
}
#
function isused(name,id,tab,line ,u,t){
# look for conflict user...
  for (u in tab){
    split(tab[u],t,":")
# if we find same user/group with different uid/gid :-( !
    if (name==t[1]&&id!=t[3]){
#	    printf "conflict %d %s %d %s \n",count++,host,id,line
      conflict[name"_"id"_"host]=line
      return 1
    }
  }
  return 0
}
'|sort -t: -nk3,1 

fi
