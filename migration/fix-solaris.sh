#!/bin/bash
umask 022
RELEASE=`uname -r`
# This script is meant to run in order to fix some things post migration

#Checking we are indeed connected to AD
CSTATUS=$(adinfo -m)
OS=$(uname -s)

if [ "${OS}" != "SunOS" ]; then
	echo "Script meant for Solaris systems"
	exit 1
fi

if [ "${CSTATUS}" != "connected" ]; then
	echo "ERROR: Not connected to AD"
	exit 1
fi  

# krb5.conf logging configuration
if [ -f /etc/krb5/krb5.conf.pre_cdc ]; then
	cat /etc/krb5/krb5.conf.pre_cdc /etc/krb5/krb5.conf > /tmp/krb5.conf
	mv 	/tmp/krb5.conf /etc/krb5/krb5.conf
	svcadm restart centrify-kcm
fi

# remove centrify cron PAM
grep -v "^adclient.autoedit.pam" /etc/centrifydc/centrifydc.conf > /tmp/centrifydc.conf.tmp
echo "adclient.autoedit.pam: false" >> /tmp/centrifydc.conf.tmp
mv /tmp/centrifydc.conf.tmp /etc/centrifydc/centrifydc.conf
adreload

egrep -v "^cron.*pam_centrifydc.so.*" /etc/pam.conf > /tmp/pam.conf.tmp
mv /tmp/pam.conf.tmp /etc/pam.conf
svcadm restart cron


# Removing teledist from /etc/passwd
grep -v "^teledist" /etc/passwd > /root/passwd.bck && mv /root/passwd.bck /etc/passwd && pwconv
# Fixing ownership of teledist
chown -R teledist:6000 /export/home/teledist > /dev/null 2>&1

#Checking ssh_admin configuration. Adding root to AllowUsers
ROOTCONF=$(grep "^AllowUsers root$" /usr/local/etc_admin)
if [ "x${ROOTCONF}" = "x" ]; then
	echo "AllowUsers root" >> /usr/local/etc_admin/sshd_config
	echo;echo
	echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	echo "WARNING: ssh_admin needs to be restarted"
	echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
fi

if [ ! -f /usr/local/etc_admin/keys/authorized_keysroot ]; then
	cp /usr/local/etc_admin/keys/authorized_keysteledist /usr/local/etc_admin/keys/authorized_keysroot
	chmod 755 /usr/local/etc_admin
	chmod 755 /usr/local/etc_admin/keys
	chmod 644 /usr/local/etc_admin/keys/authorized_keysteledist
fi

#Creating sudo shortcut
#yes|pkgrm SMCsudo > /dev/null 2>&1 
if [ -f /usr/share/centrifydc/bin/dzdo ]; then
	rm /usr/bin/sudo > /dev/null 2>&1
	ln -sf /usr/share/centrifydc/bin/dzdo /usr/bin/sudo
fi

#Running adfixid & adrmlocal

/usr/share/centrifydc/bin/adfixid -C -x -r /var/tmp/adfixid.out
/usr/share/centrifydc/bin/adrmlocal -f 

# Disabling certificate GP
chmod 666 /usr/share/centrifydc/mappers/machine/certgp.pl

#Stopping ctsa
/etc/init.d/agent-ctsa stop 
rm /etc/rc3.d/S*ctsa
pkill p_ctsce

#Stopping sysload
pkill sdclient



#Removing other accounts from password
egrep -v "^l[0-9][0-9]|^telesec|^ctsa|^app-rpm" /etc/passwd > /root/passwd.bck && mv /root/passwd.bck /etc/passwd && pwconv
egrep -v "^stdusers|^ctsa|^sysload" /etc/group > /root/group.bck && mv /root/group.bck /etc/group

#Adding nagiosr locally else nrpe does not work
adquery user nagiosr >> /etc/passwd && pwconv


# Changing default shell for Solaris
grep -v "^nss.runtime.defaultvalue.var.shell|^krb5.cache.type" /etc/centrifydc/centrifydc.conf > /tmp/centrifydc.conf.tmp
echo "nss.runtime.defaultvalue.var.shell: /bin/ksh" >> /tmp/centrifydc.conf.tmp
echo "krb5.cache.type: KCM" >> /tmp/centrifydc.conf.tmp
mv /tmp/centrifydc.conf.tmp /etc/centrifydc/centrifydc.conf
adreload
adflush

#Restarting sysload
[ -x /etc/rc3.d/S*sysload ] && /etc/rc3.d/S*sysload start

##############################################################################
#Section handling special oracle/sybase machine needs
#nsswitch.conf order change for groups. Since the oracle account has to keep its existing
#UID and GID the systems nees to map the GID correctly. We need to add files before centrifydc
RET=0
adquery user oracle > /dev/null 2>&1
if [ $? -eq 0 ]; then
	RET=1
fi
adquery user sybase > /dev/null 2>&1
if [ $? -eq 0 ]; then
	RET=1
fi
adquery user sybaseiq > /dev/null 2>&1
if [ $? -eq 0 ]; then
	RET=1
fi 

if [ ${RET} -eq 1 ]; then	
	cat /etc/nsswitch.conf|sed "s/^group.*centrifydc.*/group: files centrifydc/g" > /tmp/nsswitch.tmp
	mv /tmp/nsswitch.tmp /etc/nsswitch.conf
	chmod 644 /etc/nsswitch.conf
	grep -v "^adclient.autoedit.nss" /etc/centrifydc/centrifydc.conf > /tmp/centrifydc.conf.tmp
	echo "adclient.autoedit.nss: false" >> /tmp/centrifydc.conf.tmp
	mv -f /tmp/centrifydc.conf.tmp /etc/centrifydc/centrifydc.conf	
fi
#############################################################################

echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "Output of adfixid in /var/tmp/adfixid.out"
echo "Backup passwd in /root/CENTRIFYBACKUP/etc/passwd"
echo "Backup group in /root/CENTRIFYBACKUP/etc/group"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
