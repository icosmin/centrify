#!/bin/bash

DCPKGNAME="centrifydc-latest.rpm"
SSHPKG="centrifydc-openssh-latest.rpm"
CMKAGENT="check_mk-agent-current.rpm"
CMKLOGWCH="check_mk-agent-logwatch-current.rpm"

echo "Installing CentrifyDC"
_PKGNAME=`rpm -q CentrifyDC`
RET=$?
if [ ${RET} -eq 0 ]; then
        echo "Package already installed. Checking for upgrade"
        _PKGVER=$(rpm -q CentrifyDC)
        DCPKGVER=$(rpm -qp /root/${DCPKGNAME})
        if [ "${_PKGVER}" = "${DCPKGVER}" ]; then
        	echo "Package CentrifyDC already up-to-date."
    	else
    		# Backing up the configuration file because the new package will overwrite it
    		cp /etc/centrifydc/centrifydc.conf /root/centrifydc.conf
    		tar cvfz /root/cluster.tgz /etc/krb5.keytab /var/centrifydc/kset.*
    		
    		yum localinstall -y --nogpgcheck /root/${DCPKGNAME} > /dev/null 2>&1
    		
    		
    		# Activating in-memory kerberos cache in backup configuration file and deploying it
			# in the centrify config directory			
			cat /root/centrifydc.conf|egrep -v "^krb5.cache.type"  > /root/centrifydc.conf.tmp &&\
			echo "krb5.cache.type: KCM" >> /root/centrifydc.conf.tmp &&\
			mv /root/centrifydc.conf.tmp /root/centrifydc.conf
			cp /root/centrifydc.conf /etc/centrifydc/centrifydc.conf
			
			# Restoring kerberos keytab etc
			tar zxf /root/cluster.tgz -C /
			
			/etc/init.d/centrifydc restart > /dev/null 2>&1		
			
			chkconfig --level 23 centrify-kcm on			
    	fi
else
	
	yum localinstall -y --nogpgcheck /root/${DCPKGNAME} > /dev/null 2>&1
	echo "Checking Package installation"	
	_PKGNAME=`rpm -q CentrifyDC`
	RET=$?
	if [ "${RET}" -ne 0 ]; then
		echo "ERROR installing CentrifyDC"
		exit 1
	fi

	cp -f /etc/centrifydc/centrifydc.conf /root/centrifydc.conf

	cat /root/centrifydc.conf|egrep -v "^krb5.cache.type"  > /root/centrifydc.conf.tmp &&\
	echo "krb5.cache.type: KCM" >> /root/centrifydc.conf.tmp &&\
	mv -f /root/centrifydc.conf.tmp /root/centrifydc.conf
	cp -f /root/centrifydc.conf /etc/centrifydc/centrifydc.conf
	
	chkconfig --level 23 centrify-kcm on		
fi

if [ -f /etc/ssh/sshd_config ]; then 
	cp /etc/ssh/sshd_config /root/sshd_config
	RET=$?
fi	
if [ -f /etc/centrifydc/ssh/sshd_config ]; then
	cp /etc/centrifydc/ssh/sshd_config /root/sshd_config
	RET=$?
fi

echo "Uninstalling system ssh"
if [ ${RET} -eq 0 ]; then
	rpm -e openssh-server openssh-clients --nodeps > /dev/null 2>&1
else
	echo "ERROR backing up ssh configuration"
	exit 1;
fi

echo "Installing Centrify OpenSSH"
_PKGNAME=`rpm -q CentrifyDC-openssh`
RET=$?
if [ "${RET}" -eq 0 ]; then
	echo "Package Centrify OpenSSH already installed. Checking for upgrade"
	_SSHVER=$(rpm -q CentrifyDC-openssh)	
	SSHPKGVER=$(rpm -qp /root/${SSHPKG})
	if [ "${_SSHVER}" = "${SSHPKGVER}" ]; then
        	echo "Package Centrify OpenSSH already up-to-date."
    else
    	# Backing up the configuration file because the new package will overwrite it
    	cp /etc/centrifydc/ssh/sshd_config /root/sshd_config
    	
    	yum localinstall -y --nogpgcheck /root/${SSHPKG} > /dev/null 2>&1
    	# copy sshd_config file
    	mv /root/sshd_config /etc/centrifydc/ssh/sshd_config
    	
    	# Restarting Centrify services	
		service centrify-sshd restart
		service bnpa-ssh restart    	
	fi
else
	yum localinstall -y --nogpgcheck /root/${SSHPKG} > /dev/null 2>&1
	echo "Checking Package installation"
	_PKGNAME=`rpm -q CentrifyDC-openssh`
	RET=$?
	if [ "${RET}" -ne 0 ]; then
		echo "ERROR installing Centrify OpenSSH"
		exit 1
	fi
	# Post-instsall bnpa-ssh sshd_config file
	cat /etc/bnpa-ssh/sshd_config|egrep -v "^HostKey|^Subsystem.*sftp.*" > /tmp/bnpa-sshd_config &&\
	echo "HostKey /etc/centrifydc/ssh/ssh_host_rsa_key" >> /tmp/bnpa-sshd_config  &&\
	echo "HostKey /etc/centrifydc/ssh/ssh_host_dsa_key" >> /tmp/bnpa-sshd_config &&\
	echo "Subsystem       sftp    /usr/share/centrifydc/libexec/sftp-server" >>  /tmp/bnpa-sshd_config &&\
	mv -f /tmp/bnpa-sshd_config /etc/bnpa-ssh/sshd_config &&\
	/etc/init.d/bnpa-ssh restart

	# Post-install centrify ssh configuration
	 cp -f /root/sshd_config /etc/centrifydc/ssh/sshd_config  &&\
	 egrep -v "^ChallengeResponseAuthentication|^Subsystem.*sftp.*" /etc/centrifydc/ssh/sshd_config > /tmp/sshd_conf.tmp &&\
	 echo "ChallengeResponseAuthentication yes" >> /tmp/sshd_conf.tmp &&\
	 echo "Subsystem       sftp    /usr/share/centrifydc/libexec/sftp-server" >> /tmp/sshd_conf.tmp &&\
	 echo "DefEnvPATH /usr/share/centrifydc/sbin:/usr/share/centrifydc/kerberos/bin:/usr/share/centrifydc/kerberos/sbin:/usr/share/centrifydc/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin" >> /tmp/sshd_conf.tmp &&\ 
	 mv -f /tmp/sshd_conf.tmp /etc/centrifydc/ssh/sshd_config &&\
	 /etc/init.d/centrify-sshd stop && /etc/init.d/centrify-sshd start && \
	 ln -s /usr/share/centrifydc/sbin/sshd /usr/sbin/sshd &&\	 
	 /etc/init.d/bnpa-ssh restart	
	
	# Post-install man-path for centrify
	if [ -f /etc/man.config ]; then
		grep -v "^MANPATH.*centrify.*" /etc/man.config > /tmp/man.config && \
		echo "MANPATH /usr/share/centrifydc/man" >> /tmp/man.config && \
		mv -f /tmp/man.config /etc/man.config	
	fi
fi

echo "Installing Check-MK Agent"
_PKGNAME=`rpm -q check_mk-agent`
RET=$?
if [ "${RET}" -ne 0 ]; then
	yum localinstall -y --nogpgcheck /root/${CMKAGENT}
fi

echo "Installing Check-MK Log Watch"
_PKGNAME=`rpm -q check_mk-agent-logwatch`
RET=$?
if [ "${RET}" -ne 0 ]; then
	yum localinstall -y --nogpgcheck /root/${CMKLOGWCH}
fi

echo "Installing Check-MK plugins and local checks"
cp -r /root/cmk-local/* /usr/lib/check_mk_agent/local/
chmod 755 /usr/lib/check_mk_agent/local/*
cp -r /root/cmk-plugins/* /usr/lib/check_mk_agent/plugins/
echo 'PATH="$PATH:/usr/local/sbin"' >> /etc/sysconfig/xinetd
ln -s /opt/MegaRAID/MegaCli/MegaCli64  /usr/local/sbin/MegaCli > /dev/null 2>&1

# Cleanup
#rm -rf /root/check_mk*

#Kerberos confirguation to remove KCM logging
cp -f /root/krb5.conf /etc/krb5.conf
exit 0