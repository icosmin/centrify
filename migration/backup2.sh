#!/bin/bash
CLUSH="/usr/bin/clush"
SSHOPTS="-oConnectTimeout=5 -oStrictHostKeyChecking=no"
AWK=awk
debug=""
file=""
nodes=""
user="root"
all=0
solaris=0
dry_run="echo "


ssh teledist@${1} \
	'[ -d /root/CENTRIFYBACKUP ] && mv /root/CENTRIFYBACKUP /root/CENTRIFYBACKUP.BAK ;mkdir -p /root/CENTRIFYBACKUP/etc; cp -f /etc/passwd /root/CENTRIFYBACKUP/etc; \
	cp -f /etc/shadow /root/CENTRIFYBACKUP/etc;\
	cp -f /etc/group /root/CENTRIFYBACKUP/etc;\
	cp -r /etc/ssh /root/CENTRIFYBACKUP/etc;\
	[ -f /etc/krb5.conf ] && cp -f /etc/krb5.conf /root/CENTRIFYBACKUP/etc;\
	[ -d /etc/krb5 ] && cp -rf /etc/krb5 /root/CENTRIFYBACKUP/etc;\
	[ -f /etc/nsswitch.conf ] && cp -f /etc/nsswitch.conf /root/CENTRIFYBACKUP/etc;\
	[ -d /etc/pam.d ] && cp -rf /etc/pam.d /root/CENTRIFYBACKUP/etc;\
	[ -f /etc/pam.conf ] && cp -f /etc/pam.conf /root/CENTRIFYBACKUP/etc;\
	[ -f /usr/lib/security/methods.cfg ] && cp -f /usr/lib/security/methods.cfg /root/CENTRIFYBACKUP;\
	[ -f /etc/security/user ] && cp -f /etc/security/user /root/CENTRIFYBACKUP;';
