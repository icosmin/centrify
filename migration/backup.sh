#!/bin/bash
CLUSH="/usr/bin/clush"
SSHOPTS="-oConnectTimeout=5 -oStrictHostKeyChecking=no"
AWK=awk
debug=""
file=""
nodes=""
user="root"
all=0
solaris=0
dry_run="echo "

usage(){
  cat <<EOF
  $(basename $0) <options>
	options:	
	-h|--help 			 this help
	-w|--nodes 			 server list separate with comma
	-l|--file 			 server list in file, one per line
	-u|--user 			 user. Default: root
	-i|--ident			 identity file
	-s					 Solaris
EOF
        exit 1
}

myoptions=$(getopt -o dhi:l:su:w: -l debug,help,file:,nodes:,user:,ident: -- "$@")
[ $? -ne 0 ]&&usage

eval set -- "$myoptions"          
while true
do
  case "$1" in   
	-d| --debug) debug="echo "; shift 1;;
	-i|--ident) options="-o \"-oIdentityFile=$2\""; shift 2;;
	-l|--file) file=$2; shift 2;;
	-n|--dry_run) dry_run="echo "; shift 1;;
	-s) solaris=1; shift 1;;
	-w|--nodes) nodes=$2; shift 2;;
	-u|--user) user=$2; shift 2;;
	--) shift 1; break ;;
	-h|--help) usage
  esac
done

[ "X${debug}" = "X" ] || set -x 

if [ "X${nodes}" = "X" ] ; then
  if [ "X${file}" = "X" ] ; then
    echo "WARN: you need to provide either -w or -l switch"
    usage
  else
    if [ -f ${file} ]  ; then
      nodes=$(${AWK} '!/#/' ${file} | paste -d ',' -s)
    else
      echo "ERROR: file ${file} don't exist! "
    fi
  fi
fi

if [ ${solaris} -eq 1 -o "X${user}" = "Xteledist" ] ; then
  AWK=nawk
  user="teledist"
fi

${CLUSH} -o"${SSHOPTS}" -o"-oUser=${user}" -w ${nodes} \
	'[ -d /root/CENTRIFYBACKUP ] && mv /root/CENTRIFYBACKUP /root/CENTRIFYBACKUP.BAK ;mkdir -p /root/CENTRIFYBACKUP/etc; cp -f /etc/passwd /root/CENTRIFYBACKUP/etc; \
	cp -f /etc/shadow /root/CENTRIFYBACKUP/etc;\
	cp -f /etc/group /root/CENTRIFYBACKUP/etc;\
	cp -r /etc/ssh /root/CENTRIFYBACKUP/etc;\
	[ -f /etc/krb5.conf ] && cp -f /etc/krb5.conf /root/CENTRIFYBACKUP/etc;\
	[ -d /etc/krb5 ] && cp -rf /etc/krb5 /root/CENTRIFYBACKUP/etc;\
	[ -f /etc/nsswitch.conf ] && cp -f /etc/nsswitch.conf /root/CENTRIFYBACKUP/etc;\
	[ -d /etc/pam.d ] && cp -rf /etc/pam.d /root/CENTRIFYBACKUP/etc;\
	[ -f /etc/pam.conf ] && cp -f /etc/pam.conf /root/CENTRIFYBACKUP/etc;\
	[ -f /usr/lib/security/methods.cfg ] && cp -f /usr/lib/security/methods.cfg /root/CENTRIFYBACKUP;\
	[ -f /etc/security/user ] && cp -f /etc/security/user /root/CENTRIFYBACKUP;';
