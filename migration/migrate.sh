#!/bin/bash

TARGET=$1
TARGETFQDN=$(host ${TARGET}|head -1|awk '{print $1}')
USRNAME="root"
ARCH=""
RELEASE=$(ssh -q -oConnectTimeout=5 root@${TARGET} 'uname -r')
OS=$(ssh -q -oConnectTimeout=5 root@${TARGET} 'uname -s')
RET=$?

if [ ${RET} -ne 0 ]; then
	OS=$(ssh -q -oConnectTimeout=5 teledist@${TARGET} 'uname -s')
	RELEASE=$(ssh -q -oConnectTimeout=5 teledist@${TARGET} 'uname -r')
	ISROOT=$(ssh -q -q -oConnectTimeout=5 teledist@${TARGET} 'id teledist|grep "uid=0"')
	RET=$?	
fi

if [ "${OS}" = "SunOS" -a "${RELEASE}" != "5.11" -a "x${ISROOT}" != "x" ]; then
		USRNAME="teledist"				
fi
export TARGET USRNAME OS

if [ ${RET} -ne 0 ]; then
	echo "Could not connect to ${TARGET}"
	echo "If ${TARGET} is a Solaris server, make sure teledist can connect from linux-install-prod"
	exit 1
fi

echo "Centrify migration for ${TARGET} (${OS})"
echo
echo

# This section backs up stuff from /etc
echo -n "Backing up configuration files"
if [ "${OS}" = "SunOS" ]; then
	./backup.sh -s -w ${TARGET} > /dev/null 2>&1
	RET=$?
else
	./backup.sh -w ${TARGET} > /dev/null 2>&1
	RET=$?
fi

if [ ${RET} -ne 0 ]; then
	BCKOK=`ssh -q -oConnectTimeout=5 ${USRNAME}@${TARGET} '[ -d /root/CENTRIFYBACKUP ] && echo "OK"'`
	if [ "${BCKOK}" != "OK" ]; then
		echo
		echo "Backup failed on server ${TARGET}. Try launchin backup.sh manually."
		exit 1;
	fi	
fi

echo "	[OK]"

# This section installs the required Centrify packages on the target
echo -n "Installing Centrify packages"

if [ "${OS}" = "SunOS" ]; then
	./centrify-migr-sol.sh ${TARGET} 
	RET=$?
else 
	./centrify-migr-lin.sh ${TARGET} > /dev/null 2>&1
	RET=$?
fi
if [ ${RET} -ne 0 ]; then
	echo
	echo "An error has occured while trying to install Centrify on the target host"
	echo "Run the centrify-migr-XXX.sh script by hand"
	exit 1
fi
echo "	[OK]"

echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "TODO List:"
echo "Run \"adjoin -S -N ${TARGET} -n ${TARGETFQDN} gaia.net.intra\" on ${TARGET}"
echo "In case adjoin fails, run:" 
echo "adjoin -u svcgaiaunixjoin -c \"gaia.net.intra/Non image/BL-PROD/Servers/UnixAuth/Hosts\" -N ${TARGET} -n  ${TARGETFQDN} -z ${TARGET} gaia.net.intra"
if [ "${OS}" = "SunOS" ]; then
	echo "Run /root/fix-solaris.sh on ${TARGET}"
else
	echo "Run /root/fix-linux.sh on ${TARGET}"
fi
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
