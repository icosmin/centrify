#!/bin/bash

CENTRIFY="/bnp/files/Centrify/RedHat"
CHECKMK="/bnp/files/check_mk"
CMKAGENT="check_mk-agent-current.rpm"
CMKLOGWCH="check_mk-agent-logwatch-current.rpm"
CMKCENTRIFY="chkcentrify"
DCPKGNAME="centrifydc-latest"
SSHPKG="centrifydc-openssh-latest"
SCRIPTNAME="centrify-migrate-linux-standalone.sh"

TGT=$1

echo "Copying CentrifyDC"
scp -q ${CENTRIFY}/${DCPKGNAME} ${TGT}:/root/${DCPKGNAME}.rpm
scp -q ./fix-linux.sh ${TGT}:/root
scp -q ./centrify-migrate-linux-standalone.sh ${TGT}:/root
scp -q ./krb5.conf ${TGT}:/root
echo "Copying Centrify OpenSSH"
scp -q ${CENTRIFY}/${SSHPKG} ${TGT}:/root/${SSHPKG}.rpm
echo "Copying check_mk packages"
scp -q ${CHECKMK}/${CMKAGENT} ${TGT}:/root/${CMKAGENT}
scp -q ${CHECKMK}/${CMKLOGWCH} ${TGT}:/root/${CMKLOGWCH}
echo "Copying check_mk plugins"
scp -q -r ${CHECKMK}/local ${TGT}:/root/cmk-local
scp -q -r ${CHECKMK}/plugins ${TGT}:/root/cmk-plugins

ssh -q ${TGT} "/root/${SCRIPTNAME}"
RET=$?
if [ "${RET}" -ne 0 ]; then
	echo "ERROR deploying Centrify on ${TGT}"
	exit 1
fi