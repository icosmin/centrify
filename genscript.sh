#!/bin/bash

SCNAME=$(basename $0)
BASEDIR=$(dirname $0)
KLIST="/usr/share/centrifydc/kerberos/bin/klist"

# Checking if we already have a kerberos ticket usable for performing AD stuff
${KLIST} -s 
if [ $? -eq 0 ]; then
	export KRBSSO=1
	[ -z ${ADUSER} ] && export ADUSER=""
	[ -z ${ADPASSWD} ] && export ADPASSWD=""
	[ -z ${ADDOMAIN} ] && export ADDOMAIN="gaia.net.intra"
elif [[ -z ${ADUSER} || -z ${ADPASSWD} || -z ${ADDOMAIN} ]]; then
	echo "Please set the ADUSER, ADPASSWD and ADDOMAIN environment variables first by sourcing adlogin ( . ./adlogin)"
	exit 1
fi

if [[ $# -gt 0  &&  $1 = "-h" ]]; then
	${BASEDIR}/adtool -h|grep "${SCNAME}"
	exit 0
fi
${BASEDIR}/adtool -c ${SCNAME} $@
