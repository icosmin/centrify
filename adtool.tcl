#!/usr/bin/adedit
set _version "1.0.3"
##############################################################
# File: adtool.tcl
# Version: $_version
# Purpose: Manage Centrify and AD from the *NIX command line
# Author: Cosmin IOIART <cosmin.ioiart@bnpparibas.com>
# Copyright: BNP Paribas Arbitrage 
##############################################################
# Description:
#	This file is the user front-end. It dispatches the worker
# functions from misc.tcl
##############################################################



# Package  containing the Centrify Library
package require ade_lib

#Setting script path variables used for further includes
set _PATH [file dirname $argv0]
set _EXENAME [split [info script] "/"]
set _EXENAME [lindex $_EXENAME [llength $_EXENAME]-1]
set _copyright "$_EXENAME ver. $_version (c) 2013 BNP Paribas"
set _argv $argv

# Section for including auxilliary "library" files
source "$_PATH/etc/config.tcl"
source "$_PATH/lib/centrify.tcl"
source "$_PATH/lib/misc.tcl"
source "$_PATH/lib/datastruct.tcl"

# The env variable is used to store AD credentials and domain information
global env

# The script should only be run as non-root user
if {$env(USER) == "root"} {
	usage "$_EXENAME should not be run as root"
	exit 1
}

# Handling AD connection parameters
# domain
# ad user name
# ad password
if {[getopt argv -d domain] == 0} {
	if {! [info exists env(ADDOMAIN)]} {
		usage "Missing Domain, ex. gaia.net.intra"
		exit 1
	}

} else {
	set env(ADDOMAIN) $domain
}

if {[getopt argv -l aduser] == 0} {
	if {! [info exists env(ADUSER)]} {
		usage "Missing AD user credentials"
		exit 1
	}
} else {
	set env(ADUSER) $aduser
}

# Setting environment ADPASSWD variable which stores the AD password for the user
if {[getopt argv -p passfile] == 0} {
	global env
	set adpasswd ""

	if {[info exists env(ADPASSWD)]} {
		set adpasswd $env(ADPASSWD)
	}

	if {[string length $adpasswd] == 0} {
		puts -nonewline "Enter AD password for $aduser: "
		flush stdout
		exec stty -echo
		gets stdin adpasswd
		exec stty echo
		puts ""
		set env(ADPASSWD) "$adpasswd"
	}
	# end of if string length block
} else {
	if [catch {open $passfile r} fileId] {
		usage "Error opening password file: $fileId"
	} else {
		set adpasswd [gets $fileId]
		set env(ADPASSWD) "$adpasswd"
		close $fileId
	}
}
# end of passfile argument testing block
#========================================================================================

#parsing commands (operations)
if {[getopt argv -c command] != 0} {
	switch -exact -- $command {
		adprepare { doAdPrepare }

		precreate {doPrecreate}
		
		decomzones { doDecomZones }
	
		activateZPA {doActivateZPA }
		deactivateZPA {doDeactivateZPA }
		listzones { doListZones }

		listauth { doListAuth }

		createcmd { doCreateCmd }
		deletecmd { doDeleteCmd }
		modifycmd { doModifyCmd }
		searchcmd { doSearchCmd }

		createrole { doCreateRole }
		deleterole { doDeleteRole }
		assigncmd { doAssignCmd }	

		assignrole { doAssignRole }

		unassignrole { doUnassignRole }

		listusers -
		adduser -
		rmuser -
		moduser { doUserAction $command }

		listgroups -
		addgroup -
		rmgroup -
		modgroup { doGroupAction $command }

		listusrADgroups { doListUsrADGroups }
		listADgroup { doListADGroup }
		createADgroup { doCreateADGroup }
		deleteADgroup { doDeleteADGroup }
		addusrADgroup {doAddUserToADGroup}
		rmusrADgroup {doRmUserFromADGroup}
		
		ZPAgroups -
		ZAgroups -
		ZUnix -
		ZRolegroups { doListUnixAuthGroup $command }
		
		unlock {doUnlock}
		
		default {usage}
	}
} else {
	usage "Command is missing, ex. listzone"
	exit 1
}

