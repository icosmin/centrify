#!/bin/bash
##############################################################################
#Author: Cosmin IOIART (BNP Paribas)
#Date: 09/01/2014
#This script can be used to generate a list of accounts which need to be created 
#in Active Directory while migrating a server to Centrify.
#The output of the script is stored in the results directory
#It creates 3 files:
#1) list of accounts missing in AD which have to be created
#2) list of UID 0 accounts found (except root etc)
#3) list of accounts in passwd file which already have a Centrify profile
#
#Output number 3 can be used after activating Centrify on a machine to see if
#Centrify has been configured properly and if there arent any users missing from 
#the AD groups
#
#The ouput format is <account>: <server list>
#
##############################################################################

SCRIPTDIR=$(dirname $0)
SCRIPTDIR=$(cd "${SCRIPTDIR}" && pwd)
CURRDIR=$(pwd)
ADTOOLDIR="/bnp/tools/scripts/centrify"
CENTRIFYACCTS=${CURRDIR}/centrify-accounts.lst
SYSACCTS=${SCRIPTDIR}/system-accts.lst
AUXFILTER=${SCRIPTDIR}/aux-filter.lst
RESULTSDIR="$CURRDIR/results"
CLEANPASSWDDIR=${CURRDIR}/clean
PASSWDDIR=${CURRDIR}/passwd

[ -d $RESULTSDIR ] || mkdir ${RESULTSDIR}
rm -rf ${RESULTSDIR}/*

[ -d ${CLEANPASSWDDIR} ] || mkdir ${CLEANPASSWDDIR}
rm -rf ${CLEANPASSWDDIR}/*

# Make list of accounts available in Centrify
cd ${ADTOOLDIR}
if [[ -z ${ADUSER} || -z ${ADPASSWD} || -z ${ADDOMAIN} ]]; then
        echo "[ERROR] Please set the ADUSER, ADPASSWD and ADDOMAIN environment variables first by sourcing adlogin ( . ${ADTOOLDIR}/adlogin)"
        exit 1
fi
./listusers|egrep -v "Global|===="|awk -F: '{print $1}' > ${CENTRIFYACCTS}

# Create egrep expression for Centrify Account list
TMP=$(cat ${CENTRIFYACCTS})
CENTRLIST="^$(echo ${TMP}|sed "s/ /:|^/g"):"

TMP=$(cat ${SYSACCTS})
SYSACCTLIST="^$(echo ${TMP}|sed "s/ /:|^/g"):"

TMP=$(cat ${AUXFILTER})
AUXFLTLIST=$(echo ${TMP}|tr " " "|")

[ -d ${PASSWDDIR} ] || {
	echo "[ERROR] ${PASSWDDIR} holding the password files is missing"
	exit 1
}

echo "Using passwd information in ${PASSWDDIR}"

# Create list of all the accounts which need to be created in AD
echo "Creating list of accounts to be created in AD"
cd ${PASSWDDIR}
MISSINGACCTS=$(cat *|egrep -v "${CENTRLIST}|${SYSACCTLIST}|${AUXFLTLIST}"|awk -F: '{print $1}')
FOO=${RESULTSDIR}/ad-account-to-create.csv
rm -f ${FOO}
for i in ${MISSINGACCTS}; do
	SRVLIST=$(egrep "^${i}:" *|awk -F: '{print $1}'|sed "s/\..*//")
	echo -n "${i}: " >> ${FOO}
	for j in ${SRVLIST}; do
		echo -n "${j} " >> ${FOO}
	done
	echo >> ${FOO}
done
TMP=$(cat ${FOO}|awk -F: '{print $1}')
ADACCTS="^$(echo ${TMP}|sed "s/ /:|^/g"):"

# Create list of all the UID 0 accounts
echo "Creating list of UID 0 accounts except root and teledist"
cd ${PASSWDDIR}
UID0ACCTS=$(cat *|awk -F: '{if($3 == "0") { print $1}}'|sort -u|egrep -v "^root$|^teledist$|^app-rpm$")
FOO=${RESULTSDIR}/uid0-accounts.csv
rm -f ${FOO}
for i in ${UID0ACCTS}; do
	SRVLIST=$(egrep "^${i}:" *|awk -F: '{print $1}'|sed "s/\..*//")
	echo -n "${i}: " >> ${FOO}
	for j in ${SRVLIST}; do
		echo -n "${j} " >> ${FOO}
	done
	echo >> ${FOO}
done

# Create list of all passwd entries which have AD accounts
echo "Creating list of accounts in passwd which existing Centrify profiles"
cd ${PASSWDDIR}
ADACCTEXIST=$(cat *|egrep "${CENTRLIST}"|awk -F: '{print $1}')
FOO=${RESULTSDIR}/local-passwd-accts-with-centrify-entries.csv
rm -f ${FOO}
for i in ${ADACCTEXIST}; do
	SRVLIST=$(egrep "^${i}:" *|awk -F: '{print $1}'|sed "s/\..*//"|egrep -v "nagiosr")
	echo -n "${i}: " >> ${FOO}
	for j in ${SRVLIST}; do
		echo -n "${j} " >> ${FOO}
	done
	echo >> ${FOO}
done

#Create cleaned-up version of the passwd file
cd ${PASSWDDIR}
FLST=$(ls)
for i in ${FLST}; do
	cat ${i}|egrep -v "${ADACCTS}|${UID0ACCTS}|${CENTRLIST}" > ${CLEANPASSWDDIR}/${i}
done
