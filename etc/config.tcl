###################################################################################
###################################################################################
# Global variable definitions
# Modify the variables to suit your needs
###################################################################################
#----------------------------------------------------------------------------------
# The DN for the UnixAuth OU
set unixAuth "OU=UnixAuth,OU=Servers,OU=BL-PROD,OU=Non image"
# The DN for the Zones container under UnixAuth
set zoneContainer "OU=Zones,$unixAuth"
# The DN for the Service Accounts
set svcAcctContainer "OU=Service Accounts,$unixAuth"
# The DN for the Hosts container
set hostContainer "OU=Hosts,$unixAuth"
# The DN for the ZPA Groupd OU => where ZPA groups are stored
set zpaContainer "OU=ZPA Groups,$unixAuth"
# The DN for the AD groups meant to be used as Unix Groups
set unixGroupOU "OU=Unix Groups PlaceHolders,$unixAuth"
# The DN for AD groups meant to group accounts together for roles
set roleGroupOU "OU=Zone Role Groups,$unixAuth"
# The DN for the Zone administrator groups
set ZAGroupOU "OU=Zone Administration Groups,$unixAuth"
# The name of the Global Zone
set globalZone "GlobalZone"
# The name for the Metazone
set metaZone "Metazone"
#----------------------------------------------------------------------------------
###################################################################################
###################################################################################